//
//  Utility.swift
//  Swipy
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit


class Utility: NSObject {

    private static var instant:Utility?
    
    class var shared : Utility {
        guard let sharedInstance = self.instant else {
            let sharedInstance = Utility()
            self.instant = sharedInstance
            return sharedInstance
        }
        return sharedInstance
    }
    
    func showActivityIndicator() {
        let uiView = UIView(frame: UIScreen.main.bounds)
        let container: UIView = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.appBlackColor.withAlphaComponent(0.3)
        //            UIColorFromHex(0xffffff, alpha: 0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
        //            CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        //            UIColorFromHex(0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        //            CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.color = UIColor.appGreenColor
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        //            CGPointMake(loadingView.frame.size.width / 2,
        //                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        uiView.tag = 8956
        actInd.startAnimating()
        
        AppDelegate.shared.window?.addSubview(uiView)
    }
    
    func stopActivityIndicator() {
        if let view = AppDelegate.shared.window?.subviews.first(where: {$0.tag == 8956}) {
            view.isHidden = true
            view.removeFromSuperview()
        }
    }
}
