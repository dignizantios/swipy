//
//  LBXScanViewController.swift
//  swiftScan
//
//  Created by lbxia on 15/12/8.
//  Copyright © 2015年 xialibing. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Photos

public protocol LBXScanViewControllerDelegate: class {
     func scanFinished(scanResult: LBXScanResult, error: String?)
}

public protocol QRRectDelegate {
    func drawwed()
}

open class LBXScanViewController: UIViewController {
    
    // 返回扫码结果，也可以通过继承本控制器，改写该handleCodeResult方法即可
    open weak var scanResultDelegate: LBXScanViewControllerDelegate?

    open var delegate: QRRectDelegate?

    open var scanObj: LBXScanWrapper?

    open var scanStyle: LBXScanViewStyle? = LBXScanViewStyle()

    open var qRScanView: LBXScanView?

    // 启动区域识别功能
    open var isOpenInterestRect = false

    // 识别码的类型
    public var arrayCodeType: [AVMetadataObject.ObjectType]?

    // 是否需要识别后的当前图像
    public var isNeedCodeImage = false

    // 相机启动提示文字
    public var readyString: String! = ""

    open override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // [self.view addSubview:_qRScanView];
        view.backgroundColor = UIColor.black
        edgesForExtendedLayout = UIRectEdge(rawValue: 0)
        
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil) { _ in
            self.viewWillAppear(true)
        }
        
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Barcodes_key"),isLeftButtonShow: false)
        
        checkCameraPermission()
    }

    open func setNeedCodeImage(needCodeImg: Bool) {
        isNeedCodeImage = needCodeImg
    }

    // 设置框内识别
    open func setOpenInterestRect(isOpen: Bool) {
        isOpenInterestRect = isOpen
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        drawScanView()
        perform(#selector(LBXScanViewController.startScan), with: nil, afterDelay: 0.3)
    }

    @objc open func startScan() {
        if scanObj == nil {
            var cropRect = CGRect.zero
            if isOpenInterestRect {
                cropRect = LBXScanView.getScanRectWithPreView(preView: view, style: scanStyle!)
            }

            // 指定识别几种码
            if arrayCodeType == nil {
                arrayCodeType = [AVMetadataObject.ObjectType.qr as NSString,
                                 AVMetadataObject.ObjectType.ean13 as NSString,
                                 AVMetadataObject.ObjectType.code128 as NSString] as [AVMetadataObject.ObjectType]
            }

            scanObj = LBXScanWrapper(videoPreView: view,
                                     objType: arrayCodeType!,
                                     isCaptureImg: isNeedCodeImage,
                                     cropRect: cropRect,
                                     success: { [weak self] (arrayResult) -> Void in
                                         guard let strongSelf = self else {
                                             return
                                         }
                                         // 停止扫描动画
                                         strongSelf.qRScanView?.stopScanAnimation()
                                         strongSelf.handleCodeResult(arrayResult: arrayResult)
            })
        }

        // 结束相机等待提示
        qRScanView?.deviceStopReadying()

        // 开始扫描动画
        qRScanView?.startScanAnimation()

        // 相机运行
        scanObj?.start()
    }
    
    func checkCameraPermission(){
        obtainPermissionForMediaSourceType(sourceType: .camera,
                                           success: {
                                                
                                           }, failed: {
                                                self.showNotAuthorizedAlert(isForGallery: false)
                                           })
    }
    
    
    func showNotAuthorizedAlert(isForGallery: Bool)
        {
    //        let msg = "You have disabled X access?"
            
    //        let theMessage = NSString(format: msg as NSString, "\(isForGallery ? "Photos" : "Camera")")
            //TODO:- Change message to language support
            let theMessage = "You have disabled \(isForGallery ? "Photos" : "Camera") access"
            
            let theAlertController = UIAlertController(title: "", message: theMessage as String, preferredStyle: .alert)
            
            let openSettingsAction = UIAlertAction(title: getCommonString(key: "Open_settings_key").capitalized,
                                                   style: .default,
                                                   handler: { (alertAction) in
                                                    let theURL = URL(string: UIApplication.openSettingsURLString)
                                                        UIApplication.shared.open(theURL!, options: [:], completionHandler: { (isOpened) in })
                                                   })
            
            theAlertController.addAction(openSettingsAction)
            
//            let cancelAction = UIAlertAction(title: getCommonString(key: "Cancel_key"),
//                                             style: .default,
//                                             handler: { (alertAction) in })
//
//            theAlertController.addAction(cancelAction)
            
            self.present(theAlertController, animated: true, completion: nil)
        }
    
    private func obtainPermissionForMediaSourceType(sourceType: UIImagePickerController.SourceType,
                                                    success: @escaping () -> Void,
                                                    failed: @escaping () -> Void) {
        if sourceType == .photoLibrary || sourceType == .savedPhotosAlbum  {
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                switch authorizationStatus {
                    case .restricted:
                        failed()
                    
                    case .denied:
                        failed()
                    
                    case .authorized:
                        success()
                    
                    default:
                        break
                }
            })
        } else if sourceType == .camera {
            let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            
            switch status {
                case .authorized:
                    success()
                
                case .notDetermined:
                    AVCaptureDevice .requestAccess(for: AVMediaType.video,
                                                   completionHandler: { (isGranted) in
                                                                            if isGranted {
                                                                                success()
                                                                            } else {
                                                                                failed()
                                                                            }
                                                                      })
                
                default: // .denied .restricted
                    failed()
            }
        } else {
            assert(false, getCommonString(key: "Permission_not_found_key"))
        }
    }

    
    open func drawScanView() {
        if qRScanView == nil {
            qRScanView = LBXScanView(frame: view.frame, vstyle: scanStyle!)
            view.addSubview(qRScanView!)
            delegate?.drawwed()
        }
        qRScanView?.deviceStartReadying(readyStr: readyString)
    }
   

    /**
     处理扫码结果，如果是继承本控制器的，可以重写该方法,作出相应地处理，或者设置delegate作出相应处理
     */
    open func handleCodeResult(arrayResult: [LBXScanResult]) {
        guard let delegate = scanResultDelegate else {
            fatalError("you must set scanResultDelegate or override this method without super keyword")
        }
        navigationController?.popViewController(animated: true)
        if let result = arrayResult.first {
            delegate.scanFinished(scanResult: result, error: nil)
        } else {
            let result = LBXScanResult(str: nil, img: nil, barCodeType: nil, corner: nil)
            delegate.scanFinished(scanResult: result, error: "no scan result")
        }
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        qRScanView?.stopScanAnimation()
        scanObj?.stop()
    }
    
    @objc open func openPhotoAlbum() {
        LBXPermissions.authorizePhotoWith { [weak self] _ in
            let picker = UIImagePickerController()
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self?.present(picker, animated: true, completion: nil)
        }
    }
}

//MARK: - 图片选择代理方法
extension LBXScanViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: -----相册选择图片识别二维码 （条形码没有找到系统方法）
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        guard let image = editedImage ?? originalImage else {
            showMsg(title: nil, message: NSLocalizedString("Identify failed", comment: "Identify failed"))
            return
        }
        let arrayResult = LBXScanWrapper.recognizeQRImage(image: image)
        if !arrayResult.isEmpty {
            handleCodeResult(arrayResult: arrayResult)
        }
    }
    
}

//MARK: - 私有方法
private extension LBXScanViewController {
    
    func showMsg(title: String?, message: String?) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
}
