//
//  HomeTransactionTblCell.swift
//  Swipy
//
//  Created by YASH on 25/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class HomeTransactionTblCell: UITableViewCell {

    //MARK: - Variable
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblKwd: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblName,lblPrice,lblKwd].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlackColor
            lbl?.font = themeFont(size: 15, fontname: .bold)
        }
        
        lblKwd.font = themeFont(size: 10, fontname: .regular)
        
        lblID.textColor = UIColor.appGrayColor
        lblID.font = themeFont(size: 12, fontname: .bold)
        
        lblKwd.text = getCommonString(key: "KWD_key")
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
