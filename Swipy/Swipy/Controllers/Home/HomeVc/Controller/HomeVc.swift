//
//  HomeVc.swift
//  Swipy
//
//  Created by YASH on 21/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class HomeVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwBlue: UIView!
    @IBOutlet weak var lblCurrentAmount: UILabel!
    @IBOutlet weak var lblCurrentAmountValue: UILabel!
    @IBOutlet weak var tblTransaction: UITableView!
     @IBOutlet weak var heightTblTransaction: NSLayoutConstraint!
    
    @IBOutlet var vwHeader: UIView!
    @IBOutlet weak var lblRecentTransaction: UILabel!
    @IBOutlet weak var lblDateTransactionHeader: UILabel!
    
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Welcome_to_key")  + " Mr.X", isTitleInCenter: false, isLeftButtonShow: false,backgroundColor: UIColor.appBlueColor)
        self.navigationNotificationBtn()
        
        tblTransaction.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblTransaction.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            self.heightTblTransaction.constant = tblTransaction.contentSize.height
            
        }
    }
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        vwBlue.backgroundColor = UIColor.appBlueColor
        self.view.backgroundColor = UIColor.appBlueColor
        
        lblCurrentAmount.text = getCommonString(key: "CURRENT_AMOUNT_key")
        lblCurrentAmount.textColor = UIColor.white.withAlphaComponent(0.9)
        lblCurrentAmount.font = themeFont(size: 15, fontname: .bold)
        
        lblCurrentAmountValue.textColor = UIColor.white
        lblCurrentAmountValue.font = themeFont(size: 23, fontname: .bold)
        
        self.tblTransaction.tableHeaderView = vwHeader
        self.tblTransaction.tableFooterView = UIView()
        
        lblRecentTransaction.text = getCommonString(key: "Recent_Transactions_key")
        lblRecentTransaction.textColor = UIColor.appBlueColor
        lblRecentTransaction.font = themeFont(size: 20, fontname: .bold)
        
        lblDateTransactionHeader.text = "11 DECEMBER 2019"
        lblDateTransactionHeader.textColor = UIColor.appDarkGrayColor
        lblDateTransactionHeader.font = themeFont(size: 14, fontname: .regular)
        
        self.tblTransaction.register(UINib(nibName: "HomeTransactionTblCell", bundle: nil), forCellReuseIdentifier: "HomeTransactionTblCell")
        
    }

}

//MARK:- IBAction method
extension HomeVc{
    
    func navigationNotificationBtn(){
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightButton.setImage(UIImage(named: "ic_notification"), for: .normal)
        rightButton.tintColor = UIColor.white
        rightButton.addTarget(self, action: #selector(btnNotificationTapped), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    @objc func btnNotificationTapped(){
        let NotificationVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        NotificationVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(NotificationVC, animated: true)
    }
    
    @IBAction func btnSearchTapped(_ sender: UIButton) {
    }
    
}

//MARK: - TableView Delegate

extension HomeVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 25
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTransactionTblCell") as! HomeTransactionTblCell
        
        return cell
    }
    
}
