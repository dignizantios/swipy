//
//  AddInvoicesViewModel.swift
//  Swipy
//
//  Created by YASH on 27/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import SwiftyJSON

class AddInvoicesViewModel{
    
    //MARK: - Variable
    
    var arrayProduct : [JSON] = []
    
    var handlerTblReload:() -> Void = {}
    
    //MARK: - View life cycle
    
    init() {
        
        var dict = JSON()
        dict["product_name"].stringValue = ""
        dict["price"].stringValue = ""
        self.arrayProduct.append(dict)
    }
    
}

//MARK:- Add and remove product

extension AddInvoicesViewModel{
    
    func addProduct(){

        var dict = JSON()
        dict["product_name"].stringValue = ""
        dict["price"].stringValue = ""
        self.arrayProduct.append(dict)
        
        self.handlerTblReload()
        
    }
    
    func remomveProduct(atIndex:Int){
        self.arrayProduct.remove(at: atIndex)
        self.handlerTblReload()
    }
    
}
