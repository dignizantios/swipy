
//
//  AddInvoicesVc.swift
//  Swipy
//
//  Created by YASH on 26/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class AddInvoicesVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblClientName: CustomLabel!
    @IBOutlet weak var txtClientName: UITextField!
    
    @IBOutlet weak var lblMobileNumber: CustomLabel!
    @IBOutlet weak var txtMobileNumber: UITextField!
    
    @IBOutlet weak var lblEmail: CustomLabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblAddProduct: UILabel!
    
    @IBOutlet weak var lblTotalAmount: CustomLabel!
    @IBOutlet weak var txtTotalAmount: UITextField!
    @IBOutlet weak var lblKwd: UILabel!
    
    @IBOutlet weak var tblAddProduct: UITableView!
    @IBOutlet weak var heightTblProduct: NSLayoutConstraint!
    
    @IBOutlet weak var btnGenerateInvoice: CustomButton!
    
    //MARK: - Variable
    
    var viewModel = AddInvoicesViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        viewModel.handlerTblReload = {[weak self] in
            self?.tblAddProduct.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: "Add Invoice")
        
        tblAddProduct.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblAddProduct.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            self.heightTblProduct.constant = tblAddProduct.contentSize.height
        }
    }
    
}

//MARK: - IBAction

extension AddInvoicesVc{
    
    func setupUI(){
        
        [lblClientName,lblMobileNumber,lblEmail,lblTotalAmount].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlueColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [txtClientName,txtEmail,txtMobileNumber,txtTotalAmount].forEach { (txt) in
            txt?.textColor = UIColor.appBlueColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.delegate = self
            
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
        }
        
        lblClientName.text = getCommonString(key: "CLIENT_NAME_key")
        lblMobileNumber.text = getCommonString(key: "MOBILE_NUMBER_key")
        lblEmail.text = getCommonString(key: "EMAIL_key")
        lblTotalAmount.text = getCommonString(key: "TOTAL_AMOUNT_key")
        
        lblAddProduct.textColor = UIColor.appGrayColor
        lblAddProduct.font = themeFont(size: 13, fontname: .regular)
        lblAddProduct.text = getCommonString(key: "Add_Product_key")
        
        lblKwd.text = getCommonString(key: "KWD_key")
        lblKwd.textColor = UIColor.appBlueColor
        lblKwd.font = themeFont(size: 10, fontname: .bold)
        
        btnGenerateInvoice.backgroundColor = UIColor.appRedColor
        btnGenerateInvoice.setTitle(getCommonString(key: "GENERATE_INVOICE_key"), for: .normal)
        btnGenerateInvoice.setTitleColor(UIColor.white, for: .normal)
        btnGenerateInvoice.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        
        self.tblAddProduct.register(UINib(nibName: "AddProductTblCell", bundle: nil), forCellReuseIdentifier: "AddProductTblCell")

    }
    
    @IBAction func btnGenerateInvoicesTapped(_ sender: UIButton) {
        
        let obj = AppStoryboard.invoices.instance.instantiateViewController(withIdentifier: "GeneratedInvoicesVc") as! GeneratedInvoicesVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnAddProductTapped(_ sender: Any) {
        self.viewModel.addProduct()
    }
    
    @objc func removeProductTapped(_ sender:UIButton){
        print("Tag : \(sender.tag)")
        self.viewModel.remomveProduct(atIndex: sender.tag)
    }
    
}

//MARK: - TableView Delegate
extension AddInvoicesVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.arrayProduct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddProductTblCell") as! AddProductTblCell
        
        cell.btnRemoveProduct.isHidden = indexPath.row == 0 ? true:false
        
        cell.btnRemoveProduct.tag = indexPath.row
        cell.btnRemoveProduct.addTarget(self, action: #selector(removeProductTapped), for: .touchUpInside)
        
        cell.lblNumberProduct.text = "\(indexPath.row+1)"
        
        return cell
        
    }
    
}

//MARK: - TextField Delegate

extension AddInvoicesVc: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}
