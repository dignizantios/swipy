//
//  AddProductTblCell.swift
//  Swipy
//
//  Created by YASH on 26/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class AddProductTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblAddProduct: CustomLabel!
    @IBOutlet weak var txtProductName: UITextField!
    @IBOutlet weak var txtProductPrice: UITextField!
    @IBOutlet weak var lblKWD: UILabel!
    @IBOutlet weak var btnRemoveProduct: UIButton!
    @IBOutlet weak var lblNumberProduct: CustomLabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblAddProduct.textColor = UIColor.appBlueColor
        lblAddProduct.font = themeFont(size: 12, fontname: .regular)
        lblAddProduct.text = getCommonString(key: "Add_Product_key").uppercased()
        
        lblNumberProduct.backgroundColor = UIColor.appBlueColor
        lblNumberProduct.textColor = UIColor.white
        lblNumberProduct.font = themeFont(size: 11, fontname: .bold)
        
        lblKWD.text = getCommonString(key: "KWD_key")
        lblKWD.textColor = UIColor.appBlueColor
        lblKWD.font = themeFont(size: 10, fontname: .bold)
        
        [txtProductName,txtProductPrice].forEach { (txt) in
            txt?.textColor = UIColor.appBlueColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
