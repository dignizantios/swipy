//
//  GeneratedInvoicesVc.swift
//  Swipy
//
//  Created by YASH on 27/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class GeneratedInvoicesVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblInvoicesNumber: UILabel!
    @IBOutlet weak var lblInvoicesDate: UILabel!
    @IBOutlet weak var tblProduct: UITableView!
    @IBOutlet weak var heightTblProduct: NSLayoutConstraint!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblTotalAmountValue: UILabel!
    @IBOutlet weak var lblThankuForShopping: UILabel!
    
    @IBOutlet weak var btnPayNow: CustomButton!
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: "Invoices",backgroundColor: UIColor.appCyanBatliColor)
         tblProduct.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblProduct.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            self.heightTblProduct.constant = tblProduct.contentSize.height
        }
    }
    
}

//MARK: - SetupUI
extension GeneratedInvoicesVc{
    
    func setupUI(){
        
         self.view.backgroundColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        [lblMerchantName,lblInvoicesDate,lblInvoicesNumber,lblTotalAmount,lblTotalAmountValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlueColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        lblMerchantName.font = themeFont(size: 16, fontname: .bold)
        lblInvoicesDate.font = themeFont(size: 12, fontname: .regular)
        lblThankuForShopping.font = themeFont(size: 12, fontname: .regular)
        
        btnPayNow.backgroundColor = UIColor.appRedColor
        btnPayNow.setTitle(getCommonString(key: "PAY_NOW_key"), for: .normal)
        btnPayNow.setTitleColor(UIColor.white, for: .normal)
        btnPayNow.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        
        lblThankuForShopping.textColor = UIColor.appGrayColor
        
        lblTotalAmount.text = getCommonString(key: "TOTAL_AMOUNT_key")
        lblThankuForShopping.text = getCommonString(key: "THANK_YOU_FOR_SHOPPING_key")
        
        self.tblProduct.register(UINib(nibName: "ProductListTblCell", bundle: nil), forCellReuseIdentifier: "ProductListTblCell")
        self.tblProduct.tableFooterView = UIView()
    }
    
}

//MARK: - IBAction
extension GeneratedInvoicesVc{
    
    @IBAction func btnPayNowTapped(_ sender: UIButton) {
        
        let obj = AppStoryboard.invoices.instance.instantiateViewController(withIdentifier: "SuccessFailTransactionVc") as! SuccessFailTransactionVc
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}


//MARK: - TableView Delegate

extension GeneratedInvoicesVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListTblCell") as! ProductListTblCell

        
        return cell
    }
    
}
