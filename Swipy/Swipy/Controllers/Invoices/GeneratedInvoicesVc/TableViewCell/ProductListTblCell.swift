//
//  ProductListTblCell.swift
//  Swipy
//
//  Created by YASH on 27/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ProductListTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblProductName,lblProductPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlueColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
