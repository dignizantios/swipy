//
//  SuccessFailTransactionVc.swift
//  Swipy
//
//  Created by YASH on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SuccessFailTransactionVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var btnContinueToHome: CustomButton!
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        btnContinueToHome.backgroundColor = UIColor.appCyanBatliColor
        btnContinueToHome.setTitle(getCommonString(key: "CONTINUE_TO_HOME_key"), for: .normal)
        btnContinueToHome.setTitleColor(UIColor.white, for: .normal)
        btnContinueToHome.titleLabel?.font = themeFont(size: 15, fontname: .bold)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true) // Black screen not shown in previous screen
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func btnContinueToHomeTapped(_ sender: UIButton) {
        
        appDelegate.objCustomTabBar.selectedIndex = 0
    }
    
}
