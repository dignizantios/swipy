//
//  InvoicesVc.swift
//  Swipy
//
//  Created by YASH on 24/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class InvoicesVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblInvoices: UITableView!
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Invoices_key"), isTitleInCenter: true, isLeftButtonShow: false, backgroundColor: UIColor.appBackgroundColor,isForInvoices: true)
        
        navigationAddInvoiceBtn()
    }
    
}

//MARK:-  IBAction Method

extension InvoicesVc{
    
    func setupUI(){
        
        self.view.backgroundColor = UIColor.appBackgroundColor
        
        self.tblInvoices.register(UINib(nibName: "InvoicesTblCell", bundle: nil), forCellReuseIdentifier: "InvoicesTblCell")
        
        self.tblInvoices.tableFooterView = UIView()
        
    }
    
    func navigationAddInvoiceBtn(){
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightButton.setImage(UIImage(named: "ic_plus_green")?.withRenderingMode(.alwaysOriginal), for: .normal)
        rightButton.addTarget(self, action: #selector(btnAddInvoiceTapped), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    @objc func btnAddInvoiceTapped(){
        let obj = AppStoryboard.invoices.instance.instantiateViewController(withIdentifier: "AddInvoicesVc") as! AddInvoicesVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}

//MARK: - TableView Delegate

extension InvoicesVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoicesTblCell") as! InvoicesTblCell

        cell.btnPaidUnPaid.setTitleColor(UIColor.white, for: .normal)
        
        if indexPath.row % 2 == 0{
            cell.btnPaidUnPaid.setTitle("PAID", for: .normal)
            cell.btnPaidUnPaid.backgroundColor = UIColor.appCyanBatliColor
        }else{
            cell.btnPaidUnPaid.setTitle("UNPAID", for: .normal)
            cell.btnPaidUnPaid.backgroundColor = UIColor.appRedColor
        }
        
        return cell
    }
    
}
