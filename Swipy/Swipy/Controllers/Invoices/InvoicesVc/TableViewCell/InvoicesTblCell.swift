//
//  InvoicesTblCell.swift
//  Swipy
//
//  Created by YASH on 26/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class InvoicesTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblKWD: UILabel!
    @IBOutlet weak var btnPaidUnPaid: UIButton!
    
    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblPrice,lblKWD].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlueColor
            lbl?.font = themeFont(size: 15, fontname: .bold)
        }
        
        lblName.textColor = UIColor.appBlueColor
        lblName.font = themeFont(size: 15, fontname: .bold)
        
        lblID.textColor = UIColor.appGrayColor
        lblID.font = themeFont(size: 14, fontname: .regular)
        
        lblKWD.font = themeFont(size: 10, fontname: .regular)
        lblKWD.text = getCommonString(key: "KWD_key")
        
        btnPaidUnPaid.titleLabel?.font = themeFont(size: 13, fontname: .regular)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
