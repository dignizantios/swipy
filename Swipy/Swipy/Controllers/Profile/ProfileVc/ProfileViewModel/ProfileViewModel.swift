//
//  ProfileViewModel.swift
//  Swipy
//
//  Created by YASH on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProfileViewModel{
    
    //MARK: - Variable
    var arrayProfile : [JSON] = []
    
    //MARK: - View life cycle
    
    init() {
        createArray()
    }
}

extension ProfileViewModel{
    
    func createArray(){
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Change_Password_key")
        dict["subDetails"].stringValue = ""
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Notifications_key")
        dict["subDetails"].stringValue = getCommonString(key: "Notification_sub_msg_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Live_Chat_Support_key")
        dict["subDetails"].stringValue = ""
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Terms_and_Policy_key")
        dict["subDetails"].stringValue = ""
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Logout_key")
        dict["subDetails"].stringValue = ""
        self.arrayProfile.append(dict)
        
    }
}
