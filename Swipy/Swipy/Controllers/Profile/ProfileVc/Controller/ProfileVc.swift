//
//  ProfileVc.swift
//  Swipy
//
//  Created by YASH on 24/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ProfileVc: UIViewController {

    //MARK:- Outlet
    
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var tblProfile: UITableView!
    
    //MARK: - Variable
    
    var viewModel = ProfileViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Profile_key"),isLeftButtonShow: false)
    }
    
}

//MARK: - IBActionMethod

extension ProfileVc{
    
    func setupUI(){
        self.tblProfile.register(UINib(nibName: "ProfileTblCell", bundle: nil), forCellReuseIdentifier: "ProfileTblCell")
        self.tblProfile.tableHeaderView = vwHeader
        self.tblProfile.tableFooterView = UIView()
    }
    
    @objc func notificaitonTapped(_ sender:UIButton){
        
        sender.isSelected = !sender.isSelected
        
    }
    
    func redirectionToController(tabName:String){
        
        if tabName == getCommonString(key: "Change_Password_key"){
            let obj = AppStoryboard.profile.instance.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if tabName == getCommonString(key: "Notifications_key"){
            
        }
        else if tabName == getCommonString(key: "Live_Chat_Support_key"){
            
            let obj = AppStoryboard.profile.instance.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if tabName == getCommonString(key: "Terms_and_Policy_key"){
//            let barcodes = AppStoryboard.barcodes.instance.instantiateViewController(withIdentifier: "BarcodeDetailVc") as! BarcodeDetailVc
//            self.navigationController?.pushViewController(barcodes, animated: true)
            
            let barcodes = AppStoryboard.profile.instance.instantiateViewController(withIdentifier: "TermsAndConditionVc") as! TermsAndConditionVc
            self.navigationController?.pushViewController(barcodes, animated: true)

        }
        else if tabName == getCommonString(key: "Logout_key"){
            logoutTapped()
        }
        
    }
    
    func logoutTapped(){
        
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_you_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let vc  = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
           let rearNavigation = UINavigationController(rootViewController: vc)
           AppDelegate.shared.window?.rootViewController = rearNavigation
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}


//MARK: - TableView Delegate

extension ProfileVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arrayProfile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTblCell") as! ProfileTblCell
        
        if indexPath.row == 1{
            cell.lblSubHeader.isHidden = false
            cell.btnOnOffNotification.isHidden = false
            
            cell.btnOnOffNotification.tag = indexPath.row
            cell.btnOnOffNotification.addTarget(self, action: #selector(notificaitonTapped(_:)), for: .touchUpInside)
            
        }else{
            cell.lblSubHeader.isHidden = true
            cell.btnOnOffNotification.isHidden = true
        }
        
        let dict = viewModel.arrayProfile[indexPath.row]
        cell.lblMainHeader.text = dict["name"].stringValue
        cell.lblSubHeader.text = dict["subDetails"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = viewModel.arrayProfile[indexPath.row]
        self.redirectionToController(tabName: dict["name"].stringValue)
        
    }
}
