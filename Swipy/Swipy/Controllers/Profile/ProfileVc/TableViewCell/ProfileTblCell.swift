//
//  ProfileTblCell.swift
//  Swipy
//
//  Created by YASH on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ProfileTblCell: UITableViewCell {

    //MARK: - Variable
    
    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var btnOnOffNotification: UIButton!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblMainHeader.textColor = UIColor.appBlackColor
        lblMainHeader.font = themeFont(size: 15, fontname: .regular)
        
        lblSubHeader.textColor = UIColor.appGrayColor
        lblSubHeader.font = themeFont(size: 12, fontname: .regular)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
