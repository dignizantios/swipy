//
//  ChangePasswordVc.swift
//  Swipy
//
//  Created by Yash on 30/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ChangePasswordVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblEnterYourOldPassword: UILabel!
    @IBOutlet weak var txtEnterOldPassword: CustomTextField!
    
    @IBOutlet weak var lblEnterYourNewPassword: UILabel!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    
    @IBOutlet weak var lblConfirmNewPassword: UILabel!
    @IBOutlet weak var txtConfirmNewPassword: CustomTextField!
    
    @IBOutlet weak var btnUpdate: CustomButton!
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Change_Password_key"))
        
    }
    
}

//MARK: - Setupui

extension ChangePasswordVc{
    
    func setupUI(){
        
        [lblConfirmNewPassword,lblEnterYourOldPassword,lblEnterYourNewPassword].forEach { (lbl) in
            lbl?.textColor = UIColor.appGrayColor
            lbl?.font = themeFont(size: 13, fontname: .regular)
        }
        
        [txtConfirmNewPassword,txtEnterOldPassword,txtNewPassword].forEach { (txt) in
            txt?.textColor = UIColor.appBlackColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.delegate = self
            txt?.leftPaddingView = 20
            txt?.rightPaddingView = 20
            
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
        }
        
        lblEnterYourOldPassword.text = getCommonString(key: "Enter_your_old_password_key")
        lblEnterYourNewPassword.text = getCommonString(key: "Enter_your_new_password_key")
        lblConfirmNewPassword.text = getCommonString(key: "Confirm_your_new_password_key")
        
        self.btnUpdate.backgroundColor = UIColor.appCyanBatliColor
        self.btnUpdate.setTitle(getCommonString(key: "UPDATE_key"), for: .normal)
        self.btnUpdate.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        self.btnUpdate.setTitleColor(UIColor.white, for: .normal)

    }
    
}

//MARK: - IBAction method

extension ChangePasswordVc{
    
    @IBAction func btnUpdateTapped(_ sender: UIButton) {
    }
    
}

//MARK: - TextField Delegate

extension ChangePasswordVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}

