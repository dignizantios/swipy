//
//  SupportVC.swift
//  Swipy
//
//  Created by Abhay on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SupportVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblSupport: UITableView!
    @IBOutlet weak var txtMsg: CustomTextview!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var constantBottomTxtVw: NSLayoutConstraint!
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Support_key"))
        
        //Register Notification for keyboard hide/show
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool)
    {

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

}

//MARK: - Action

extension SupportVC{
    
    func setupUI(){
        
        self.tblSupport.register(UINib(nibName: "LeftSupportCell", bundle: nil), forCellReuseIdentifier: "LeftSupportCell")
        
        self.tblSupport.register(UINib(nibName: "SupportRightCell", bundle: nil), forCellReuseIdentifier: "SupportRightCell")
        
        self.tblSupport.tableFooterView = UIView()
        
        if GlobalVariables.isEnglish{
            txtMsg.textAlignment = .left
        }else{
            txtMsg.textAlignment = .right
            btnSend.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        
    }
}

//MARK: - TableView Delegate

extension SupportVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftSupportCell") as! LeftSupportCell
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportRightCell") as! SupportRightCell
        
        return cell
    }
    
}

//MARK: - Keyboard Show/Hide

extension SupportVC
{
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        {
            let keyboardHeight = keyboardRectValue.height
            
            print("keyBorad Height : \(keyboardHeight) ")
            
            UIView.animate(withDuration: 0.5, animations: {
                self.constantBottomTxtVw.constant = keyboardHeight
                //   self.view.layoutIfNeeded()
                
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.constantBottomTxtVw.constant = 0.0
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
    
}

//MARK: - TextField delegate

extension SupportVC : UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        if textView.text == ""
        {
            textView.text = ""
            self.lblMsg.isHidden = false
            self.btnSend.isUserInteractionEnabled = false
            self.btnSend.alpha = 0.5
        }
        else
        {
            self.lblMsg.isHidden = true
            self.btnSend.isUserInteractionEnabled = true
            self.btnSend.alpha = 1
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            txtMsg.resignFirstResponder()
            return false
        }
        return true
    }
    
}
