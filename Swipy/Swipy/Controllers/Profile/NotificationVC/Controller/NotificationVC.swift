//
//  NotificationVC.swift
//  Swipy
//
//  Created by Abhay on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationVC: UIViewController {
    
    //#MARK:- Outlets
    @IBOutlet weak var tblNotification: UITableView!
    
    //#MARK:- Declaration
    var Notificationarray:[JSON] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Notifications_key"))
        setupUI()
    }
    
    func setupUI(){
        self.tblNotification.register(UINib(nibName: "NotificationHeaderCell", bundle: nil), forCellReuseIdentifier: "NotificationHeaderCell")
        self.tblNotification.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        var dic = JSON()
        var arr:[JSON] = []
        dic["title"].stringValue = "TODAY"
        var dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00 "
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dic["value"] = JSON(arr)
        Notificationarray.append(dic)
        dic = JSON()
        arr.removeAll()
        arr = [JSON]()
        dic["title"].stringValue = "YESTERDAY"
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dic["value"] = JSON(arr)
        Notificationarray.append(dic)
        dic = JSON()
        arr.removeAll()
        arr = [JSON]()
        dic["title"].stringValue = "THIS WEEKEND"
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dict = JSON()
        dict["name"].stringValue = "Dianne Ameter"
        dict["time"].stringValue = "10:45 AM"
        dict["msg"].stringValue = "You recieved a payment of KWD:750.00"
        arr.append(dict)
        dic["value"] = JSON(arr)
        Notificationarray.append(dic)
        tblNotification.reloadData()
    }
    

}
//MARK: - TableView Delegate
extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Notificationarray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Notificationarray[section]["value"].arrayValue.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderCell") as! NotificationHeaderCell
        cell.lblHeader.text = Notificationarray[section]["title"].stringValue
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        let value = Notificationarray[indexPath.section]["value"].arrayValue
        let dic = value[indexPath.row]
        cell.lblName.text = dic["name"].stringValue
        cell.lblTime.text = dic["time"].stringValue
        cell.lblMessage.text = dic["msg"].stringValue
        return cell
        
    }
    
}
