//
//  NotificationHeaderCell.swift
//  Swipy
//
//  Created by Abhay on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class NotificationHeaderCell: UITableViewCell {
    @IBOutlet weak var lblHeader: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblHeader.textColor = UIColor.appBlackColor
        lblHeader.font = themeFont(size: 14, fontname: .regular)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
