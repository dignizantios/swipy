//
//  NotificationCell.swift
//  Swipy
//
//  Created by Abhay on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var imgNotification: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblName,lblTime,lblMessage].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlackColor
        }
        lblName.font = themeFont(size: 14, fontname: .light)
        lblTime.font = themeFont(size: 14, fontname: .bold)
        lblMessage.font = themeFont(size: 14, fontname: .regular)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
