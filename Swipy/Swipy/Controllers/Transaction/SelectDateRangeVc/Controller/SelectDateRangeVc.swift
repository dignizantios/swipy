//
//  SelectDateRangeVc.swift
//  Swipy
//
//  Created by YASH on 25/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SelectDateRangeVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblTransaction: UITableView!
    @IBOutlet weak var vwBottomPdf: UIView!
    @IBOutlet weak var btnDownloadPDF: CustomButton!
    
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var txtFrom: CustomTextField!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var txtTo: CustomTextField!
    @IBOutlet weak var btnGenerateReport: CustomButton!
    
    //MARK: - Outlet
    
    var isSelectFromDate = false
    var selectedFromDate = Date()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Select_Transaction_Period_key"))
    }
    
    func setupUI(){
        self.tblTransaction.register(UINib(nibName: "TransactionTblCell", bundle: nil), forCellReuseIdentifier: "TransactionTblCell")
        self.tblTransaction.tableFooterView = UIView()
        self.vwBottomPdf.backgroundColor = UIColor.appBlueColor
        self.view.backgroundColor = UIColor.appBlueColor
        self.btnDownloadPDF.setTitleColor(UIColor.white, for: .normal)
        self.btnDownloadPDF.setTitle(getCommonString(key: "DOWNLOAD_AS_PDF_key"), for: .normal)
        self.btnDownloadPDF.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        
        self.btnGenerateReport.backgroundColor = UIColor.appBlueColor
        self.btnGenerateReport.setTitle(getCommonString(key: "GENERATE_REPORT_key"), for: .normal)
        self.btnGenerateReport.titleLabel?.font = themeFont(size: 16, fontname: .bold)
        self.btnGenerateReport.setTitleColor(UIColor.white, for: .normal)
        
        [self.lblFrom,self.lblTo].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlueColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [self.txtFrom,self.txtTo].forEach { (txt) in
            txt?.textColor = UIColor.appBlueColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.delegate = self
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
        }
        
        self.lblFrom.text = getCommonString(key: "From_key")
        self.lblTo.text = getCommonString(key: "To_key")
        
    }
    
}

//MARK: - IBAction metod

extension SelectDateRangeVc{
    
    @IBAction func btnDownloadPDFTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func btnGenerateReportTaped(_ sender: UIButton) {
    }
}

//MARK: - TextField Delegate

extension SelectDateRangeVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.txtFrom{
            
            self.isSelectFromDate = true
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.datePickerCustomMode.pickerMode = .date
            obj.isSetMaximumDate = true
            obj.maximumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            
            self.present(obj, animated: false, completion: nil)
            return false
        }
        else if textField == self.txtTo{
            
//            if txtFrom.text?.count == 0{
//                makeToast(strMessage: getCommonString(key: "Please_select"))
//                return false
//            }
            
            self.isSelectFromDate = false
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.datePickerCustomMode.pickerMode = .date
            obj.isSetMinimumDate = true
            obj.minimumDate = selectedFromDate
            obj.isSetMaximumDate = true
            obj.maximumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            
            self.present(obj, animated: false, completion: nil)
            return false
        }
        return true
    }
}


//MARK: - TableView Delegate

extension SelectDateRangeVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTblCell") as! TransactionTblCell
        
        return cell
    }
    
}

//MARK: - Datepicker delegate

extension SelectDateRangeVc : datePickerDelegate
{
    func setDateValue(dateValue: Date) {
        
        print("dateValue : \(dateValue)")
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if isSelectFromDate // Date Selection
        {
            selectedFromDate = dateValue
            txtFrom.text = dateFormatter.string(from: dateValue)
            txtTo.text = ""
        }else{
            txtTo.text = dateFormatter.string(from: dateValue)
        }
        
    }
    
    
}
