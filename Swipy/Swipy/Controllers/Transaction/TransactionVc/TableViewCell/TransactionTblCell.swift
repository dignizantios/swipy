//
//  TransactionTblCell.swift
//  Swipy
//
//  Created by YASH on 25/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class TransactionTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblKwd: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblPrice,lblKwd].forEach { (lbl) in
            lbl?.textColor = UIColor.appGreenColor
            lbl?.font = themeFont(size: 15, fontname: .bold)
        }
        
        lblName.textColor = UIColor.appBlackColor
        lblName.font = themeFont(size: 15, fontname: .bold)
        
        lblID.textColor = UIColor.appGrayColor
        lblID.font = themeFont(size: 12, fontname: .bold)
        
        lblKwd.font = themeFont(size: 10, fontname: .regular)
        lblKwd.text = getCommonString(key: "KWD_key")
        
        lblDate.textColor = UIColor.appGrayColor
        lblDate.font = themeFont(size: 10, fontname: .bold)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
