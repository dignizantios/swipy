//
//  TransactionVc.swift
//  Swipy
//
//  Created by YASH on 24/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import CarbonKit

class TransactionVc: UIViewController {

    //MARK:- Oulet
    
    @IBOutlet weak var vwTransactionMain: UIView!
    @IBOutlet weak var btnToday: CustomButton!
    @IBOutlet weak var btnYesterDay: CustomButton!
    @IBOutlet weak var tblTransaction: UITableView!
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Transaction_key") ,isTitleInCenter: false,isLeftButtonShow: false)
    }
    
    func setupUI(){
        
        btnToday.setTitle(getCommonString(key: "Today_key"), for: .normal)
        btnYesterDay.setTitle(getCommonString(key: "Yesterday_key"), for: .normal)
        
        [btnToday,btnYesterDay].forEach { (btn) in
            btn?.setTitleColor(UIColor.appGrayColor, for: .normal)
            btn?.setTitleColor(UIColor.appGrayColor, for: .selected)
        }
        
        self.selectedBtnColorAndFontType(selected: btnToday, unSelected: btnYesterDay)
        
        self.tblTransaction.register(UINib(nibName: "TransactionTblCell", bundle: nil), forCellReuseIdentifier: "TransactionTblCell")
        
        self.tblTransaction.tableFooterView = UIView()
        
    }
    
}

//MARK: - IBAction method

extension TransactionVc
{
    @IBAction func btnTodayTapped(_ sender: UIButton) {
        selectedBtnColorAndFontType(selected: btnToday, unSelected: btnYesterDay)
    }
    
    @IBAction func btnYesterdayTapped(_ sender: UIButton) {
        selectedBtnColorAndFontType(selected: btnYesterDay, unSelected: btnToday)
    }
    
    func selectedBtnColorAndFontType(selected:CustomButton,unSelected:CustomButton){
        
        selected.isSelected = true
        unSelected.isSelected = false
        
        selected.titleLabel?.font = themeFont(size: 16, fontname: .bold)
        unSelected.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        
        self.tblTransaction.reloadData()
    }
    
    
    @IBAction func btnSelectRangeTapped(_ sender: UIButton) {
        
        let obj = AppStoryboard.transaction.instance.instantiateViewController(withIdentifier: "SelectDateRangeVc") as! SelectDateRangeVc
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}

//MARK: - TableView Delegate

extension TransactionVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if btnToday.isSelected{
            return 10
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTblCell") as! TransactionTblCell
        
        if btnToday.isSelected{
            
        }else{
            
        }
        
        return cell
    }
    
}
