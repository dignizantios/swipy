//
//  BarCodeDetailViewMdoel.swift
//  Swipy
//
//  Created by Yash on 31/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import SwiftyJSON

class BarCodeDetailViewModel{
    
    //MARK: - Variabl
    
    var array : [JSON] = []
    
    var handlerReloadTbl:() -> Void = {}
    
    //MARK: - View life cycle
    init() {
        self.createArray()
    }
    
}

extension BarCodeDetailViewModel{
    
    func createArray(){
        
        var dict = JSON()
        dict["qty"].intValue = 1
        self.array.append(dict)
        
        dict = JSON()
        dict["qty"].intValue = 2
        self.array.append(dict)
        
        dict = JSON()
        dict["qty"].intValue = 2
        self.array.append(dict)
        
    }
    
    func addPrdouct(index:Int){
        
        var dict = array[index]
        dict["qty"].intValue += 1
        array[index] = dict
       // self.handlerReloadTbl()
    }
    
    func minusProduct(index:Int){
        var dict = array[index]
        if dict["qty"].intValue > 1 {
            dict["qty"].intValue -= 1
        }
        array[index] = dict
       // self.handlerReloadTbl()
    }
    
}
