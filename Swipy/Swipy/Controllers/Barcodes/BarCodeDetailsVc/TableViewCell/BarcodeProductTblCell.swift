//
//  BarcodeProductTblCell.swift
//  Swipy
//
//  Created by Yash on 31/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class BarcodeProductTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var vwPlusMinus: CustomView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    
    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [vwMain,vwPlusMinus].forEach { (vw) in
            vw?.backgroundColor = UIColor(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        }
        
        lblProductName.font = themeFont(size: 12, fontname: .regular)
        lblBrandName.font = themeFont(size: 10, fontname: .regular)
        lblPrice.font = themeFont(size: 11, fontname: .bold)
        
        lblQuantity.font = themeFont(size: 10, fontname: .regular)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
