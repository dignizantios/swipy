//
//  BarcodeDetailVc.swift
//  Swipy
//
//  Created by Yash on 31/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class BarcodeDetailVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblBarcodeProduct: UITableView!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet var vwFooter: UIView!
    @IBOutlet weak var btnContinue: CustomButton!
    
    //MARK: - Variable
    
    var viewModel = BarCodeDetailViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Barcode_Scan_key"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        
        let obj = AppStoryboard.invoices.instance.instantiateViewController(withIdentifier: "GeneratedInvoicesVc") as! GeneratedInvoicesVc
        self.navigationController?.pushViewController(obj, animated: true)
                                                             
    }
}

//MARK: - SetupUI

extension BarcodeDetailVc{
    
    func setupUI(){
        self.view.backgroundColor = UIColor(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1.0)

        self.tblBarcodeProduct.register(UINib(nibName: "BarcodeProductTblCell", bundle: nil), forCellReuseIdentifier: "BarcodeProductTblCell")
        self.tblBarcodeProduct.tableFooterView = vwFooter
        
        self.btnContinue.backgroundColor = UIColor.appCyanBatliColor
        self.btnContinue.setTitle(getCommonString(key: "CONTINUE_key"), for: .normal)
        self.btnContinue.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        self.btnContinue.setTitleColor(UIColor.white, for: .normal)

        self.tblBarcodeProduct.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 25, right: 0)
        
    }
    
}

//MARK: - IBAction method

extension BarcodeDetailVc{
    
    @IBAction func btnScanTapped(_ sender: UIButton) {
           
    }
    
    @objc func btnPlusProductTapped(_ sender:UIButton){
        viewModel.addPrdouct(index: sender.tag)
        self.tblBarcodeProduct.reloadData()
//        viewModel.handlerReloadTbl = {[weak self] in
//            self?.tblBarcodeProduct.reloadData()
//        }
    }
    
    @objc func btnMinusProductTapped(_ sender:UIButton){
        viewModel.minusProduct(index: sender.tag)
        self.tblBarcodeProduct.reloadData()
//        viewModel.handlerReloadTbl = {[weak self] in
//            self?.tblBarcodeProduct.reloadData()
//        }
    }
}

//MARK: - TableView Delegate

extension BarcodeDetailVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarcodeProductTblCell") as! BarcodeProductTblCell
        
        let dict = viewModel.array[indexPath.row]
        
        cell.lblQuantity.text = dict["qty"].stringValue
        
        [cell.btnPlus,cell.btnMinus].forEach { (btn) in
            btn?.tag = indexPath.row
        }
        
        cell.btnPlus.addTarget(self, action: #selector(btnPlusProductTapped), for: .touchUpInside)
        cell.btnMinus.addTarget(self, action: #selector(btnMinusProductTapped), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
