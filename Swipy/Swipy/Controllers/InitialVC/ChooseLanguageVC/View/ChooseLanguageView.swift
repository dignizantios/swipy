//
//  ChooseLanguageView.swift
//  Swipy
//
//  Created by Abhay on 21/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ChooseLanguageView: UIView {
    
    @IBOutlet weak var lblChooseLang: UILabel!
    @IBOutlet weak var btnArabic: CustomButton!
    @IBOutlet weak var btnEnglish: CustomButton!
    
    //MARK:- Functions
    func setupUI(theController: ChooseLanguageVC) {
        theController.view.backgroundColor = UIColor.appBlueColor
        [lblChooseLang].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 14, fontname: .bold)
            lbl?.text = getCommonString(key: "CHOOSE_YOUR_LANGUAGE_key").uppercased()
        }
        [btnArabic,].forEach { (btn) in
            btn?.titleLabel?.textColor = UIColor.white
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .bold)
            btn?.backgroundColor = UIColor.appBlueColor
        }
        btnArabic.setTitle(getCommonString(key: "Arabic_key").uppercased(), for: .normal)
        btnEnglish.setTitle(getCommonString(key: "English_key").uppercased(), for: .normal)
    }
}
