//
//  ChooseLanguageVC.swift
//  Swipy
//
//  Created by Abhay on 21/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ChooseLanguageVC: UIViewController {
    //MARK:- Variables
    lazy var mainView: ChooseLanguageView = { [unowned self] in
        return self.view as! ChooseLanguageView
        }()
    
    lazy var mainModelView: ChooseLanguageViewModel = {
        return ChooseLanguageViewModel(theController: self)
    }()

    
    var isEnglish = false
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.mainView.setupUI(theController: self)
    }
    
    //#MARK: - Button Action
    
    @IBAction func btnArabicAction(_ sender: UIButton) {
        setLanguage(token: "ar")
        
        self.isEnglish = false
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        navigateToLanguage()

    }
    
    @IBAction func btnEnglishAction(_ sender: UIButton) {
        setLanguage(token: "en")
        
        self.isEnglish = true
        
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        navigateToLanguage()
        
    }
    
    func navigateToLanguage(){
               
        let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TourVC") as! TourVC

        let mov = UINavigationController(rootViewController: login)
        
        if self.isEnglish{
            
            mov.navigationBar.semanticContentAttribute = .forceLeftToRight
        }else{
            mov.navigationBar.semanticContentAttribute = .forceRightToLeft
        }
        
        appDelegate.window?.rootViewController = mov
        
    }
    
}
