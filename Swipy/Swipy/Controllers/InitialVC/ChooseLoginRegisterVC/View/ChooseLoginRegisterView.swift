
//
//  ChooseLoginRegisterView.swift
//  Swipy
//
//  Created by Abhay on 21/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

class ChooseLoginRegisterView: UIView {
    
    //MARK: Outlets
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnRegister: UIButton!
    
    //MARK:- Functions
    func setupUI(theController: ChooseLoginRegisterVC) {
        btnLogin.setTitle(getCommonString(key: "Login_Key").uppercased(), for: .normal)
        btnRegister.setTitle(getCommonString(key: "Register_key").uppercased(), for: .normal)
        
        [btnLogin,btnRegister].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .regular)
            btn?.setTitleColor(UIColor.white, for: .normal)
        }
    }
}
