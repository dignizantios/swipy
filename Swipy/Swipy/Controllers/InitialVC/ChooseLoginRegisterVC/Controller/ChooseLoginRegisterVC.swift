//
//  ChooseLoginRegisterVC.swift
//  Swipy
//
//  Created by Abhay on 21/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class ChooseLoginRegisterVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: ChooseLoginRegisterView = { [unowned self] in
        return self.view as! ChooseLoginRegisterView
        }()
    
    var viewModel = ChooseLoginRegisterModelView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.handler = {[weak self] (str) in
            //strErrorMsg = str
            //MTLVisibilityResultMode
        }
        
        // Do any additional setup after loading the view.
        self.mainView.setupUI(theController: self)
    }
    
    //#MARK: - Button Action

    @IBAction func btnRegisterAction(_ sender:UIButton){
        let SignupVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(SignupVC, animated: true)
    }
    @IBAction func btnLoginAction(_ sender:UIButton){
        let LoginVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(LoginVC, animated: true)
    }

}
