//
//  LoginVC.swift
//  Swipy
//
//  Created by Abhay on 27/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    //#MARK:- Outlets
    
    @IBOutlet weak var lblLoginToContinue: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var imgCheckUnCheckKeepMeSignIn: UIImageView!
    @IBOutlet weak var lblKeepMeSignIn: UILabel!
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var lblDontHaveAc: UILabel!
    
    //#MARK:- Declaration
    var isKeepMeLogin = Bool()
    
    //#MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //#MARK:- Button Action
    
    @IBAction func btnKeepMeLogin(_ sender: UIButton) {
        if isKeepMeLogin{
            isKeepMeLogin = false
            imgCheckUnCheckKeepMeSignIn.image = UIImage(named: "ic_check_boc_unselect")
        }else{
            imgCheckUnCheckKeepMeSignIn.image = UIImage(named: "ic_check_boc_select")
            isKeepMeLogin = true
        }
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
         appDelegate.objCustomTabBar = TabbarController()
        self.navigationController?.pushViewController(appDelegate.objCustomTabBar, animated: false)
    }
    
    @IBAction func btnDontHaveAcAction(_ sender: UIButton) {
        let SignupVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(SignupVC, animated: true)
    }

}
//MARK:- UI Setup

extension LoginVC{
    
    func setupUI(){
        
        [lblKeepMeSignIn].forEach { (lbl) in
            lbl?.textColor = UIColor.appBlackColor
            lbl?.font = themeFont(size: 12, fontname: .light)
        }
        
        [txtEmail,txtPassword].forEach { (txt) in
            txt?.textColor = UIColor.appBlueColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.delegate = self
            
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
            
        }
        txtEmail.placeholder = getCommonString(key: "Email_id_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        lblKeepMeSignIn.text = getCommonString(key: "Keep_me_signiin_key")
        lblDontHaveAc.text = "\(getCommonString(key: "Dont_have_account_key")) \(getCommonString(key: "SIGNUP_key")))"
        
        lblLoginToContinue.textColor = UIColor.black
        lblLoginToContinue.font = themeFont(size: 16, fontname: .bold)
        lblLoginToContinue.text = getCommonString(key: "LogIn_to_continue_key")
        
        
        btnLogin.backgroundColor = UIColor.appCyanBatliColor
        btnLogin.setTitle(getCommonString(key: "Login_Key"), for: .normal)
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnLogin.titleLabel?.font = themeFont(size: 16, fontname: .regular)
       
        
        let DontHaveText = getCommonString(key: "Dont_have_account_key")
        let SIGNUPText = getCommonString(key: "SIGNUP_key")
        //lblDontHaveAc.setSubTextColor(pSubString: SIGNUPText, pColor: UIColor.red)
        
        let boldFont:UIFont = themeFont(size: 16.0, fontname: .regular)
        let firstfont:UIFont = themeFont(size: 12.0, fontname: .light)
        
        let firstDict:NSDictionary = NSDictionary(object: firstfont, forKey:
            NSAttributedString.Key.font as NSCopying)
        let attributedString = NSMutableAttributedString(string: DontHaveText,
                                                         attributes: firstDict as? [NSAttributedString.Key : Any])
        let boldDict:NSDictionary = NSDictionary(object: boldFont, forKey:
            NSAttributedString.Key.font as NSCopying)
        
        let boldString = NSMutableAttributedString(string:SIGNUPText,
                                                   attributes:boldDict as? [NSAttributedString.Key : Any])
        attributedString.append(boldString)
        
        attributedString.setColor(color: UIColor.appRedColor, forText: SIGNUPText)
        lblDontHaveAc.attributedText = attributedString
        
        
    }
        
}

//MARK: - TextField Delegate

extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            return txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword{
            return self.view.endEditing(true)
        }
        return true
    }
    
}
