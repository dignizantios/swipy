//
//  SignupVC.swift
//  Swipy
//
//  Created by Abhay on 27/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    //#MARK:- Outlets
    
    @IBOutlet weak var lblSignupHere: UILabel!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    @IBOutlet weak var btnSignup: CustomButton!
    @IBOutlet weak var lblBySigningUp: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         setupUI()
        self.addInputAccessoryForTextFields(textFields: [txtName,txtPhoneNumber,txtEmail,txtPassword,txtConfirmPassword], dismissable: true, previousNextable: true)
    }
    

    //#MARK:- Button Action
    
    @IBAction func btnSignupAction(_ sender: UIButton) {
        let SetPinVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SetPinVC") as! SetPinVC
        self.navigationController?.pushViewController(SetPinVC, animated: true)
    }

}
//MARK:- UI Setup

extension SignupVC{
    
    func setupUI(){
        
        
        [txtName,txtPhoneNumber,txtEmail,txtPassword,txtConfirmPassword].forEach { (txt) in
            txt?.textColor = UIColor.appBlueColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.delegate = self
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
        }
        txtName.placeholder = getCommonString(key: "Name_key")
        txtPhoneNumber.placeholder = getCommonString(key: "Phone_number_key")
        txtEmail.placeholder = getCommonString(key: "Email_id_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        txtConfirmPassword.placeholder = getCommonString(key: "Confirm_password_key")
        
        lblSignupHere.textColor = UIColor.black
        lblSignupHere.font = themeFont(size: 16, fontname: .bold)
        lblSignupHere.text = getCommonString(key: "Signup_here_now_key")
        
        
        btnSignup.backgroundColor = UIColor.appRedColor
        btnSignup.setTitle(getCommonString(key: "SIGNUP_key"), for: .normal)
        btnSignup.setTitleColor(UIColor.white, for: .normal)
        btnSignup.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        
        //lblBySigningUp.text = getCommonString(key: "By_signingup_key")
        lblBySigningUp.textColor =  UIColor.appBlackColor
        lblBySigningUp.font = themeFont(size: 12, fontname: .light)
        setUIForAgreeTermsCondition()
    }
    //MARK:- Terms& Condition
    
    func setUIForAgreeTermsCondition()
        
    {
        let str1 = getCommonString(key: "By_signingup_key")
        let str2 = getCommonString(key: "Terms_key")
        let str3 = getCommonString(key: "Condition_key")
        let And = getCommonString(key: "and_key")
        let str4 = getCommonString(key: "Of_the_app_key")
        
        
        let str = str1 + " " + str2 + " \(And) " + str3 + " \(str4)"
        let interactableText = NSMutableAttributedString(string:str)
        let rangeTerms = (str as NSString).range(of: str2, options: .caseInsensitive)
        
        let rangePolicy = (str as NSString).range(of: str3, options: .caseInsensitive)
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      
                                      value: themeFont(size: 12, fontname: .light),
                                      
                                      range: NSRange(location: 0, length: interactableText.length))
        
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      
                                      value: UIColor.appRedColor, range: rangeTerms)
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      
                                      value: UIColor.appRedColor, range: rangePolicy)
        
       // interactableText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeTerms)
       // interactableText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangePolicy)
        
        lblBySigningUp.attributedText = interactableText
        
        let tapGeture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        lblBySigningUp.addGestureRecognizer(tapGeture)
        
    }
    @objc func tapLabel(_ gesture: UITapGestureRecognizer)
        
    {
        
        let str1 = getCommonString(key: "By_signingup_key")
        let str2 = getCommonString(key: "Terms_key")
        let str3 = getCommonString(key: "Condition_key")
        let And = getCommonString(key: "and_key")
        let str4 = getCommonString(key: "Of_the_app_key")
        
        
        let str = str1 + " " + str2 + " \(And) " + str3 + " \(str4)"
        let rangeTerms = (str as NSString).range(of: str2, options: .caseInsensitive)
        
        let rangePolicy = (str as NSString).range(of: str3, options: .caseInsensitive)
        
        
        let checkClickedTerms = gesture.didTapAttributedTextInLabel(label: lblBySigningUp,inRange : rangeTerms)
        
        if(checkClickedTerms)
        {
            print("Tapped terms")
            
        }
        
        let checkClickedPrivacy = gesture.didTapAttributedTextInLabel(label: lblBySigningUp,inRange : rangePolicy)
        
        if(checkClickedPrivacy)
            
        {
            print("Tapped condition")
            
            
        }
        
    }
}

//MARK: - TextField Delegate

extension SignupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtName{
            return txtPhoneNumber.becomeFirstResponder()
        }
        else if textField == txtPhoneNumber{
            return txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail{
            return txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword{
            return txtConfirmPassword.becomeFirstResponder()
        }
        else if textField == txtConfirmPassword{
            return self.view.endEditing(true)
        }
        return true
    }
    
}
