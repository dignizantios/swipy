//
//  TourVC.swift
//  Swipy
//
//  Created by Abhay on 21/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class TourVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: TourView = { [unowned self] in
        return self.view as! TourView
        }()
    
    lazy var mainModelView: TourModelView = {
        return TourModelView(theController: self)
    }()
    
    //MARK: Variables
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.setupUI(theController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //UIApplication.shared.statusBarView?.backgroundColor = .white
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //UIApplication.shared.statusBarView?.backgroundColor = ColorNavigationStatusBar()
    }
    @IBAction func btnSkipAction(_ sender: UIButton) {
        let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseLoginRegisterVC") as! ChooseLoginRegisterVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
        let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseLoginRegisterVC") as! ChooseLoginRegisterVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    
}

