//
//  TourCollectionCell.swift
//  Taal
//
//  Created by Vishal on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class TourCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var imgTitle: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 24, fontname: .regular)
            lbl?.textColor = UIColor.appBlackColor
        }
        
        [lblDescription].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.appBlackColor
        }
    }

}
