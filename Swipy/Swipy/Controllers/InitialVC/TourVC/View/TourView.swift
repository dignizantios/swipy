//
//  TourView.swift
//  Swipy
//
//  Created by Abhay on 21/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

class TourView: UIView {
    
    //MARK: Outlets
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var vwBottom: UIView!
    @IBOutlet var btnSkip: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var pageController: UIPageControl!
    
    //MARK:- Functions
    func setupUI(theController: TourVC) {
        btnSkip.setTitle(getCommonString(key: "Skip_key").uppercased(), for: .normal)
        btnNext.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        
        [btnSkip,btnNext].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .regular)
            btn?.setTitleColor(UIColor.appBlackColor, for: .normal)
        }
        collectionView.register(UINib(nibName: "TourCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TourCollectionCell")
        collectionView.reloadData()
        
        pageController.hidesForSinglePage = true
        pageController.pageIndicatorTintColor = UIColor.lightGray
        pageController.currentPageIndicatorTintColor = UIColor.appBlackColor
        pageController.numberOfPages = 1
    }
}
