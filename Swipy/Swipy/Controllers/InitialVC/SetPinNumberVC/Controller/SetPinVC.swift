//
//  SetPinVC.swift
//  Swipy
//
//  Created by Abhay on 28/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class SetPinVC: UIViewController {

    //#MARK:- Outlets
    
    @IBOutlet weak var lblEnterYourPin: UILabel!
    @IBOutlet weak var txtPinNumber: CustomTextField!
    @IBOutlet weak var btnHideShowPwd: UIButton!
    
    //#MARK:- Declaration
    var isPwdShow = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    //#MARK:- Button Actions
    
    @IBAction func btnHideShowPwdAction(_ sender: UIButton) {
        if isPwdShow{
            btnHideShowPwd.setImage(UIImage(named: "ic_eye_on"), for: .normal)
            isPwdShow = false
            txtPinNumber.isSecureTextEntry = true
            
            
        }else{
            btnHideShowPwd.setImage(UIImage(named: "ic_eye_close"), for: .normal)
            isPwdShow = true
            txtPinNumber.isSecureTextEntry = false
        }
        
    }

}
//MARK:- UI Setup

extension SetPinVC{
    
    func setupUI(){
        
        [lblEnterYourPin].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 16, fontname: .bold)
            lbl?.text = getCommonString(key: "Enter_your_pin_number_key").uppercased()
        }
        
        [txtPinNumber].forEach { (txt) in
            txt?.textColor = UIColor.white
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.delegate = self
            if GlobalVariables.isEnglish{
                txt?.textAlignment = .left
            }else{
                txt?.textAlignment = .right
            }
        }
        txtPinNumber.placeholder = getCommonString(key: "Pin_number_key").uppercased()
        self.addInputAccessoryForTextFields(textFields: [txtPinNumber], dismissable: true, previousNextable: true)
    }
    
}

//MARK: - TextField Delegate

extension SetPinVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         if textField == txtPinNumber{
            return self.view.endEditing(true)
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textstring.characters.count
        if length > 4 {
            return false
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if txtPinNumber.text != ""{
            appDelegate.objCustomTabBar = TabbarController()
            self.navigationController?.pushViewController(appDelegate.objCustomTabBar, animated: false)
        }
        return true
    }
}
