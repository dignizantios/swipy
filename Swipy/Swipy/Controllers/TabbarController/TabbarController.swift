//
//  TabbarController.swift
//  Swipy
//
//  Created by YASH on 21/12/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController,UITabBarControllerDelegate {
    
    //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self

        self.tabBar.tintColor = UIColor.gray
        self.tabBar.unselectedItemTintColor = UIColor.gray
        
        let home = AppStoryboard.home.instance.instantiateViewController(withIdentifier: "HomeVc") as! HomeVc
        let tabbarOneItem = UITabBarItem(title: getCommonString(key: "Home_key").uppercased(), image: UIImage(named:"ic_home_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_home")?.withRenderingMode(.alwaysOriginal))
        home.tabBarItem = tabbarOneItem
        
        let transaction = AppStoryboard.transaction.instance.instantiateViewController(withIdentifier: "TransactionVc") as! TransactionVc
        let tabbarTwoItem = UITabBarItem(title: getCommonString(key: "Transaction_key").uppercased(), image: UIImage(named:"ic_transactions_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_transactions_select")?.withRenderingMode(.alwaysOriginal))
        transaction.tabBarItem = tabbarTwoItem
        
        let invoices = AppStoryboard.invoices.instance.instantiateViewController(withIdentifier: "InvoicesVc") as! InvoicesVc
        let tabbarThreeItem = UITabBarItem(title: getCommonString(key: "Invoices_key").uppercased(), image: UIImage(named:"ic_invoice_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_invoice_select")?.withRenderingMode(.alwaysOriginal))
        invoices.tabBarItem = tabbarThreeItem
        
        let barcodes = AppStoryboard.barcodes.instance.instantiateViewController(withIdentifier: "LBXScanViewController") as! LBXScanViewController
        //let barcodes = LBXScanViewController()
        let tabbarFourItem = UITabBarItem(title: getCommonString(key: "Barcodes_key").uppercased(), image: UIImage(named:"ic_barcode_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_barcode_select")?.withRenderingMode(.alwaysOriginal))
        barcodes.tabBarItem = tabbarFourItem
        
        let profile = AppStoryboard.profile.instance.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
        let tabbarFiveItem = UITabBarItem(title: getCommonString(key: "Profile_key").uppercased(), image: UIImage(named:"ic_profile_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_profile_select")?.withRenderingMode(.alwaysOriginal))
        profile.tabBarItem = tabbarFiveItem
        
        var style = LBXScanViewStyle()
        style.centerUpOffset = 44
        style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle.Inner
        style.photoframeLineW = 2
        style.photoframeAngleW = 18
        style.photoframeAngleH = 18
        style.isNeedShowRetangle = false
        style.anmiationStyle = LBXScanViewAnimationStyle.LineMove
        style.colorAngle = UIColor(red: 0.0/255, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        barcodes.scanStyle = style
        barcodes.scanResultDelegate = self
        
        let n1 = UINavigationController(rootViewController:home)
        let n2 = UINavigationController(rootViewController:transaction)
        let n3 = UINavigationController(rootViewController:invoices)
        let n4 = UINavigationController(rootViewController:barcodes)
        let n5 = UINavigationController(rootViewController:profile)
        
        n1.isNavigationBarHidden = false
        n2.isNavigationBarHidden = false
        n3.isNavigationBarHidden = false
        n4.isNavigationBarHidden = false
        n5.isNavigationBarHidden = false
        
        self.viewControllers = [n1,n2,n3,n4,n5]
        self.tabBar.backgroundColor = UIColor.white
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let navigationController = viewController as? UINavigationController
        navigationController?.popToRootViewController(animated: true)
        
    }
    
}

extension TabbarController: LBXScanViewControllerDelegate{
    
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        
        if error != nil{
            print("error:\(error)")
            return
        }
        
        print("Scan Result :\(scanResult.strScanned)")
        
        let barcodes = AppStoryboard.barcodes.instance.instantiateViewController(withIdentifier: "BarcodeDetailVc") as! BarcodeDetailVc
        
        self.navigationController?.pushViewController(barcodes, animated: true)
        
        
    }
    
}
