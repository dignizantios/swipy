//
//  Basicstuff.swift
//  RentASki Support
//
//  Created by Abhay on 22/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import MaterialComponents
import CoreLocation

struct GlobalVariables {
    static var localTimeZoneName: String { return TimeZone.current.identifier }
    static let deviceType = "iOS"
    static let isTypeHost = Bool()
    static var userCurrentLocation : CLLocation?
    static var securityNumber = 4
    static var accountNumber = 16
    static var cvv = 3
    static var isEnglish = false
} 

let Defaults = UserDefaults.standard
let user_key = "user"
let user_data_key = "user_data"
let is_remember_key = "is_remember"
let remember_email_key = "remember_email"
let remember_password_key = "remember_password"
let is_firsttime_open_key = "is_firsttime_open"
//let is_studentlist_firsttime_key = "is_studentlist_firsttime"
//let is_billinglist_firsttime_key = "is_billinglist_firsttime"

let appDelegate = UIApplication.shared.delegate as! AppDelegate
var isTypeHost = Bool()
var navImage = UIImage()
var SideMenuSelectedIndex = Int()

////Check ideal time logout fucntionality
var timeOutValue = 600.0
var kCheckActiveTime = "checkActiveTime"

func getFormatedDate(date: Date) -> String
{
    let currentTimeStamp = DateToString(Formatter: "yyyyMMddHHmmss", date: date)
    return currentTimeStamp
}
func DateToString(Formatter : String,date : Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    let convertedString = dateformatter.string(from: date)
    return convertedString
}
public func setLanguage(token: String)
{
    if token == "ar"{
        GlobalVariables.isEnglish = false
    }else{
        GlobalVariables.isEnglish = true
    }
    
    UserDefaults.standard.setValue(token, forKey: "LanguageKey")
    UserDefaults.standard.synchronize()
}
public func getLanguage() -> String
{
    if UserDefaults.standard.value(forKey: "LanguageKey") == nil{
        setLanguage(token: "en")
    }
    return UserDefaults.standard.value(forKey: "LanguageKey") as! String
}

//MARK: - Setup mapping
let StringFilePath = Bundle.main.path(forResource: "Language", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)

func getCommonString(key:String) -> String {
    return dictStrings?.object(forKey: key) as? String ?? ""
}

//MARK: - Set Toaster

func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)    
}
/*
func getUserDetail() -> LoginModelClass {
    let userDetail = loadJSON(key: USER_DETAILS_KEY)
    let data = JSON(userDetail)
    return LoginModelClass(JSON: data.dictionaryObject!)!
}
*/
/*
///--- Access token data for non user ---
func saveGenerateAccessToken(token:JSON) {
    guard let rawData = try? token.rawData() else { return }
    Defaults.set(rawData, forKey: GENERATE_ACCESS_TOKEN)
    Defaults.synchronize()
    
}

func getAccessTokenData() -> GenerateAccessTokenModel? {
    if let token = Defaults.object(forKey: GENERATE_ACCESS_TOKEN) as? Data, let json = JSON(token).dictionaryObject {
        return GenerateAccessTokenModel(JSON: json)
    }
    return nil
}

func removeAccessTokenData() {
    Defaults.removeObject(forKey: GENERATE_ACCESS_TOKEN)
    Defaults.synchronize()
}




*/
/*
///--- User data ---
func getUserData() -> SignupModelClass?{
    if let user = Defaults.object(forKey: user_data_key) as? Data,let json = JSON(user).dictionaryObject{
       // print(JSON(user))
       // print(json)
        return SignupModelClass(JSON: json)
    }
    return nil
}
*/
func savedUserData(user: JSON) {
    guard let rawData = try? user.rawData() else { return }
    Defaults.set(rawData, forKey: user_data_key)
    Defaults.synchronize()
}

func removeUserData() {
    Defaults.removeObject(forKey: user_data_key)
    Defaults.synchronize()
}

func printFonts() {
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName )
        print("Font Names = [\(names)]")
    }
}
