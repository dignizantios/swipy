//
//  APIKey.swift
//  Swipy
//
//  Created by Khushbu on 09/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation

struct APIKey {
    
    static let key_success = "success"
    static let key_msg = "message"
    static let key_fields = "fields"
    static let key_api_key = "api_key"
    
    
    ///----- Login
    
    static let key_username = "username"
    static let key_password = "password"
    static let key_password_verify = "password_verify"
    static let key_persist_session = "persist_session"
    
    static let key_timezone = "timezone"

    
    ///----- Register
    
    static let key_plan_id = "plan_id"
    static let key_first_name = "first"
    static let key_last_name = "last"
    static let key_email = "email"
    static let key_parent_accept_email_reminders = "parent_accept_email_reminders"
    
    static let key_newsletter = "newsletter"
    static let key_subdomain = "subdomain"
    static let key_studio_name = "studio_name"
    static let key_addres = "address"
    static let key_city = "city"
    static let key_state = "state"
    static let key_zip = "zip"
    static let key_country = "country"
    static let key_apt = "apt"
    //static let key_profile_image = "profile_image"
    static let key_user_type = "user_type"
    
    // ----- Student
    
    static let key_student_login_id = "student_login_id"
    static let key_student_first = "student_first"
    static let key_student_last = "student_last"
    static let key_student_email = "student_email"
    static let key_student_accept_email_reminders = "accept_email_reminders"
    static let key_student_phone = "student_phone"
    static let key_student_username = "student_username"
    static let key_student_password = "student_password"
    static let key_rate_name = "rate_name"
    static let key_rate_id = "rate_id"
    static let key_skype = "skype"
    static let key_notes = "notes"
    
     ///----- Get StudentList
    static let key_contact_id = "contact_id"
    static let key_teacher_id = "teacher_id"
    static let key_student_id = "student_id"
    static let key_status = "status"
    static let key_token = "token"
    static let key_id = "id"
    static let key_offset = "offset"
    static let key_flp_id = "flp_id"
    static let key_phone = "phone"
    
    //Edit Student Profile
    static let key_flp_ids = "flp_ids"
    static let key_type = "type"
    static let key_login_id = "login_id"
    static let key_lesson_cost = "lesson_cost"
    static let key_birthday = "birthday"
    static let key_school = "school"
    static let key_instrument = "instrument"
    static let key_skill_level = "skill_level"
    static let key_billing_method = "billing_method"
    
    ///----- Get EventList
    static let key_permission = "permission"
    static let key_start_date = "start_date"
    static let key_end_date = "end_date"
    static let key_limit = "limit"
    static let key_order = "order"
    static let key_date = "date"
    
    ///-------
    static let key_lang_value = "0"
    static let key_access_token = "access_token"
    static let key_full_name = "full_name"
    static let key_address = "address"
    static let key_date_of_birth = "date_of_birth"
    static let key_gender = "gender"
    
    ///--- User Type
    static let key_teacher = "teacher"
    static let key_student = "student"
    static let key_flp = "flp"
}
