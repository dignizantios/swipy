//
//  ApiConstant.swift
//  MTH
//
//  Created by Abhay on 29/10/18.
//  Copyright © 2018 Abhay. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftyJSON
import Alamofire

//MARK: - Main URLS
//let SERVER_URL = "https://musicteachershelper.com/mobile"

let SERVER_URL = "https://sandbox.musicteachershelper.com/mobile"
let BASE_URL = SERVER_URL + "/api"

let auth = "MTH:UzuyQh,Sb+g9754S"
let authData = auth.data(using: String.Encoding.utf8)!
let base64EncodedCredential = authData.base64EncodedString()
//    .base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
let authString = "Basic \(base64EncodedCredential)"
//let authorization = ["Authorization" : authString]
let authorization = ["version" : app_version]

//MARK: - URLS Names

let REGISTER = "/api_user/register"
let LOGIN = "/api_user/login"
let FORGOTPWD = "/api_user/forgot"
//let FORGOTPWD = "/api_user/resend_verification"
let EVENTDATES = "/api_calendar/event_dates"
let ALLSTUDENTLIST = "/api_student/list"
let ALLPAYMENTHISTORYLIST = "/api_transaction/list"
let ONESTUDENTPAYMENTHISTORYLIST = "/api_transaction/billing_history"
let ALLEVENTLIST = "/api_calendar/list"
let EVENTBYDATE = "/api_calendar/events_by_date"
let EVENTDETAILS = "/api_calendar/fetch"
let STUDENTDETAIL = "/api_student/fetch"
let TEACHERPROFILE = "/api_teacher/fetch"
let STUDENTPROFILE = "/api_student/fetch"
let FLPPROFILE = "/api_flp/fetch"
let EDITTEACHERPROFILE = "/api_teacher/update"
let EDITSTUDENTPROFILE = "/api_student/editstudent"
let EDITFLPPROFILE = "/api_flp/edit"
let UPCOMINGEVENTS = "/api_calendar/upcoming_events"
let HISTORYEVENTS = "/api_calendar/history_events"
let RATEPACKAGELIST = "/api_rate_package/list"
let EXISTINGFLPLIST = "/api_flp/list"
let INSTRUMENTSLIST = "/api_user/instruments"
let COUNTRYLISTLIST = "/api_user/fetch_countries"
let CHECKUSERNAME = "/api_student/validate_username"
let LOGOUT = "/api_user/logout"
let CHECKEMAIL = "/api_user/check_email"
let CHECKSUBDOMAIN = "/api_user/check_subdomain"
let CHECKSTUDIONAME = "/api_user/check_studio_name"
let CHECKTOKEN = "/api_user/check"
let ADDSTUDENTS = "/api_student/addstudent"

enum APIKeys: String {
    case success = "success"
    case status = "status"
    case message = "message"
}


extension JSON {
    func getString(key: APIKeys) -> String {
        return self[key.rawValue].string ?? ""
    }
    
    func getInt(key: APIKeys) -> Int {
        return self[key.rawValue].int ?? 0
    }
    
    func getBool(key: APIKeys) -> Bool {
        return self[key.rawValue].bool ?? false
    }
    
    func getDouble(key: APIKeys) -> Double {
        return self[key.rawValue].double ?? 0.0
    }
    
    func getFloat(key: APIKeys) -> Float {
        return self[key.rawValue].float ?? 0.0
    }
    
    func getDictionary(key: APIKeys) -> JSON {
        return JSON(self[key.rawValue].dictionary ?? [String: JSON]())
    }
    
    func getArray(key: APIKeys) -> [JSON] {
        return self[key.rawValue].array ?? [JSON]()
    }
    
    mutating func setValue(key: APIKeys, value: String) {
        self[key.rawValue].string = value
    }
    
    mutating func setIntValue(key: APIKeys, value: Int) {
        self[key.rawValue].int = value
    }
    
    mutating func setBoolValue(key: APIKeys, value: Bool) {
        self[key.rawValue].bool = value
    }
}
struct AlamofireManager {
    
    static let shared: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        
        let sessionManager = Alamofire.SessionManager(configuration: configuration, delegate: SessionDelegate(), serverTrustPolicyManager: nil)
        return sessionManager
    }()
}

struct WebServices {
    
    func cancelledAllRequest() {
        Alamofire.SessionManager.default.session.getAllTasks { (urlSessionTasks) in
            urlSessionTasks.forEach({$0.cancel()})
        }
    }
    func cancelledCurrentRequestifExist(url:String) {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadDataTask, downloadDataTask) in
            if let index = sessionDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                sessionDataTask[index].cancel()
            }
            if let index = uploadDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                uploadDataTask[index].cancel()
            }
            if let index = downloadDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                downloadDataTask[index].cancel()
            }
            
        }
    }
    func get(endpoint:String ,parameter:[String:Any],completion:@escaping(Result<JSON>) -> Void) {
        let url = BASE_URL + endpoint
        
        var para = parameter
        para[APIKey.key_api_key] = api_value
        /*para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_register_id] = push_token
        para[APIKey.key_lang] = lang*/
        
        Alamofire.request(url, method: .get, parameters: para, encoding: URLEncoding.default, headers: authorization).responseSwiftyJSON { (result) in
            print("url:=",url)
            print("parameter:=",para)
            print("authorization:=",authorization)
            print("result:=",result )
            print("response:=",result.value ?? "nil")
            completion(result.result)
        }
    }
    
    func post(endpoint:String ,parameter:[String:Any],completion:@escaping(Result<JSON>) -> Void) {
        let url = BASE_URL + endpoint
        
        //        print("URL:", url)
        var para = parameter
        para[APIKey.key_api_key] = api_value
        /*
        para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_register_id] = push_token
        para[APIKey.key_lang] = lang*/
        print(para)
        print("authorization:=",authorization)
        Alamofire.request(url, method: .post, parameters: para, encoding: URLEncoding.httpBody, headers: authorization).responseSwiftyJSON { (result) in
            print("statusCode:=",result.response?.statusCode ?? "nil")
            print("authorization:=",authorization)
            print("url:=",url)
            print("parameter:=",para)
            print("result:=",result)
            print("response:=",result.value ?? "nil")
            completion(result.result)
        }
    }
    
    func postUsingMultiPartData(endpoint:String ,parameter:[String:Any],files:[Data],withName:[String],withFileName:[String],mimeType:[String],completion:@escaping(JSON) -> Void) {
        let url = BASE_URL + endpoint
        
        var para = parameter
        para[APIKey.key_api_key] = api_value
       /* para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_register_id] = push_token
        para[APIKey.key_lang] = lang*/
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            var index = 0
            for data in files {
                multipartFormData.append(data, withName: withName[index], fileName: withFileName[index] , mimeType: mimeType[index])
                index += 1
            }
            //            print("multipartFormData:=",multipartFormData.)
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: authorization) { (result) in
            print("authorization:=",authorization)
            print("url:=",url)
            print("parameter:=",para)
            
            switch result {
            case .success(let upload, _, _):
                print("statusCode:=",upload.response?.statusCode ?? "nil")
                upload.responseSwiftyJSON(completionHandler: { (result) in
                    if let json = result.result.value {
                        print("json:=",json)
                        completion(json)
                    } else {
                        completion(JSON(["flag":"0","msg":getCommonString(key: "Something_went_wrong_key")]))
                    }
                })
                break
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                var msg = getCommonString(key: "Something_went_wrong_key")
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    msg = getCommonString(key: "No_internet_connection_key")
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    msg = getCommonString(key: "Request_time_out_Please_try_again")
                }
                let result = JSON(["flag":"0","msg":msg])
                completion(result)
                
                break
            }
        }
    }
    
    func put(endpoint:String ,parameter:[String:Any],completion:@escaping(Result<JSON>) -> Void) {
        let url = BASE_URL + endpoint
        
        var para = parameter
        para[APIKey.key_api_key] = api_value
       /* para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_register_id] = push_token
        para[APIKey.key_lang] = lang*/
        
        Alamofire.request(url, method: .put, parameters: para, encoding: URLEncoding.default, headers: authorization).responseSwiftyJSON { (result) in
            print("url:=",url)
            print("parameter:=",para)
            print("response:=",result.value ?? "nil")
            completion(result.result)
        }
    }
    func downloadFileatDestination(audioUrl:String,completion:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        let documentsUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // your destination file url
        let endurl = URL.init(string: audioUrl)!
        let destination = documentsUrl.appendingPathComponent(endurl.lastPathComponent.replacingOccurrences(of: ".wav", with: ".mp3"))
        print(destination)
        // check if it exists before downloading it
        if FileManager().fileExists(atPath: destination.path) {
            print("The file already exists at path")
            completion(destination.path)
            return
        } else {
            //  if the file doesn't exist
            //  just download the data from your url
            URLSession.shared.downloadTask(with: endurl, completionHandler: { (location, response, error) in
                // after downloading your data you need to save it to your destination url
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let location = location, error == nil
                    else {
                        var msg = getCommonString(key: "Something_went_wrong_key")
                        if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                            // no internet connection
                            msg = getCommonString(key: "No_internet_connection_key")
                            
                        } else if let err = error as? URLError, err.code == URLError.timedOut {
                            msg = getCommonString(key: "Request_time_out_Please_try_again")
                        }
                        //                let result = JSON(["flag":"0","msg":msg])
                        failed(msg)
                        return
                }
                
                do {
                    try FileManager.default.moveItem(at: location, to: destination)
                    print("file saved")
                    completion(destination.absoluteString)
                    return
                } catch {
                    print(error)
                }
            }).resume()
        }
    }
    
    func downloadData(url:String,completion:@escaping(Data?) -> Void,failed:@escaping(String) -> Void)  {
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: nil).responseData { (response) in
            switch response.result {
            case .success(let fileData):
                completion(fileData)
                break
            case .failure(let error):
                print("error:=",error)
                var msg = getCommonString(key: "Something_went_wrong_key")
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    msg = getCommonString(key: "No_internet_connection_key")
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    msg = getCommonString(key: "Request_time_out_Please_try_again")
                }
                //                let result = JSON(["flag":"0","msg":msg])
                failed(msg)
                break
            }
        }
        /*
         Alamofire.download(url).responseData { (response) in
         switch response.result {
         case .success(let fileData):
         completion(fileData)
         break
         case .failure(let error):
         var msg = ValidationTexts.SOMETHING_WENT_WRONG
         if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
         // no internet connection
         msg = ValidationTexts.NO_INTERNET_CONNECTION
         
         } else if let err = error as? URLError, err.code == URLError.timedOut {
         msg = ValidationTexts.REQUEST_TIME_OUT
         }
         //                let result = JSON(["flag":"0","msg":msg])
         failed(msg)
         break
         }
         }*/
    }
    
    
}
