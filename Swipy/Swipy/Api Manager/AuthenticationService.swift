//
//  AuthenticationService.swift
//  Swipy
//
//  Created by Khushbu on 10/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class AuthenticationService: NSObject {
    
    static let shared = AuthenticationService()
    /*
    func generateAccessToken(parameter:[String:Any], success:@escaping(_ token : GenerateAccessTokenModel,_ data:JSON) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().post(endpoint: GENERATE_ACCESS_TOKEN, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_flag")].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)

                        if let token = GenerateAccessTokenModel(JSON: data) {
                            success(token, JSON(data))
                            return
                        }
                    }
                }
                failed(value[getCommonString(key: "key_msg")].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(getCommonString(key: "No_internet_connection_key"))
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                break
            }
        }
    }
    
    func login(parameters:[String:Any], success:@escaping(_ token: LoginModelClass,_ data: JSON) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().post(endpoint: LOGIN, parameter: parameters) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_flag")].intValue == 0 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        
                        if let token = LoginModelClass(JSON: data) {
                            success(token, JSON(data))
                            return
                        }
                    }
                }
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    // no internet connection
                    failed(getCommonString(key: "No_internet_connection_key"))
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                break
            }
        }
    }
    
    func signUp(parameters:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping(_ token: SignupModelClass,_ data: JSON) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        //["profile_image"], withFileName: ["image1"], mimeType: ["multipart/form-data"]
            
        WebServices().postUsingMultiPartData(endpoint: REGISTER, parameter: parameters, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            if result[getCommonString(key: "key_flag")].intValue == 1 {
                if let data = result.dictionaryObject {
                    print("data:=",data)
                    
                    if let token = SignupModelClass(JSON: data) {
                        success(token, JSON(data))
                        return
                    }
                }
                failed(result[APIKey.key_msg].stringValue)
                
                //                success()
                return
            } else {
                
                failed(result[APIKey.key_msg].stringValue)
                return
            }
            //            failed(ValidationTexts.SOMETHING_WENT_WRONG)
        
        }
        
        
        
    }
    
    func verifyOTP(parameters:[String:Any], success:@escaping(_ token:VerifyOtpModelClass,_ data: JSON) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().post(endpoint: VERIFY_OTP, parameter: parameters) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_flag")].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:",data)
                        
                        if let token = VerifyOtpModelClass(JSON: data) {
                            success(token, JSON(data))
                            return
                        }
                    }
                }
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    // no internet connection
                    failed(getCommonString(key: "No_internet_connection_key"))
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                break
            }
        }
    }
    func resendOTP(parameters:[String:Any], success:@escaping(_ token:ResentOTPModelClass,_ data:JSON) -> Void, failed:@escaping(String) -> Void) {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().post(endpoint: RESEND_OTP, parameter: parameters) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_flag")].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("Data:", data)
                        
                        if let token = ResentOTPModelClass(JSON: data) {
                            success(token, JSON(data))
                            return
                        }
                    }
                }
                if value[getCommonString(key: "key_flag")].intValue == 0 {
                    if let data = value.dictionaryObject {
                        print("Data:", data)
                        makeToast(strMessage: value[getCommonString(key: "key_msg")].stringValue)
                        Utility.shared.stopActivityIndicator()
                    }
                }
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    // no internet connection
                    failed(getCommonString(key: "No_internet_connection_key"))
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                break
            }
        }
    }
    func uploadLicense(parameters:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping(_ token:UploadLicenseModelClass,_ data:JSON) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().postUsingMultiPartData(endpoint: UPLOAD_LICENSE, parameter: parameters, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            if result[getCommonString(key: "key_flag")].intValue == 1 {
                if let data = result.dictionaryObject {
                    print("data:=",data)
                    
                    if let token = UploadLicenseModelClass(JSON: data) {
                        success(token, JSON(data))
                        return
                    }
                }
                if let data = result.dictionaryObject {
                    print("Data:", data)
                    makeToast(strMessage: result[getCommonString(key: "key_msg")].stringValue)
                    Utility.shared.stopActivityIndicator()
                }
                
                
                failed(result[APIKey.key_msg].stringValue)
                
                //                success()
                return
            } else {
                
                failed(result[APIKey.key_msg].stringValue)
                return
            }
            //            failed(ValidationTexts.SOMETHING_WENT_WRONG)
            
        }
       /* WebServices().post(endpoint: UPLOAD_LICENSE, parameter: parameters) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_flag")].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("Data:",data)
                        
                        if let token = UploadLicenseModelClass(JSON: data) {
                            success(token, JSON(data))
                            return
                        }
                    }
                }
                if value[getCommonString(key: "key_flag")].intValue == 0 {
                    if let data = value.dictionaryObject {
                        print("Data:", data)
                        makeToast(strMessage: value[getCommonString(key: "key_msg")].stringValue)
                        Utility.shared.stopActivityIndicator()
                    }
                }
                
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    // no internet connection
                    failed(getCommonString(key: "No_internet_connection_key"))
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                break
            }
        }*/
    }
    */
    
    func logout(parameters:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().post(endpoint: LOGOUT, parameter: parameters) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_flag")].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    // no internet connection
                    makeToast(strMessage: getCommonString(key: "No_internet_connection_key"))
                    failed(getCommonString(key: "No_internet_connection_key"))
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    makeToast(strMessage: getCommonString(key: "Request_time_out_Please_try_again"))
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    makeToast(strMessage: getCommonString(key: "Something_went_wrong_key"))
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                Utility.shared.stopActivityIndicator()
                break
            }
        }
    }
}
