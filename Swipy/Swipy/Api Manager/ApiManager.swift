//
//  ApiManager.swift
//  RentASki
//
//  Created by Abhay on 29/10/18.
//  Copyright © 2018 Abhay. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import Reachability
import SystemConfiguration

struct AlamofireAppManager {
    static let shared: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        configuration.allowsCellularAccess = true
//        configuration.httpMaximumConnectionsPerHost = 1
        configuration.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData

        if #available(iOS 11.0, *) {
            configuration.waitsForConnectivity = true
        } else {}
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
}

class ApiManager: NSObject {
    
    static var shared = ApiManager()
    
    func showProgress(vc: UIViewController) {
        let LoaderString:String = "Loading..."
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        
        vc.startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
    }
    
    func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
    //MARK: - Post
    func MakePostAPI(name:String, params:[String:Any], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void) {        

        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: getCommonString(key: "No_internet_connection_key"))
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        var headers:[String : String] = [:]
        /*if getUserDetail().success != nil {
            let base64Credentials = (getUserDetail().data?.tokenType)! + " " + (getUserDetail().data?.accessToken)!
            headers = ["Authorization": base64Credentials, "Content-Type": "application/json"]
        }
        else {
            headers = ["Content-Type": "application/json"]
        }*/
        print(headers)
        
        let url = BASE_URL + name
        print(url)
        print(params)
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            
            if progress {
                vc.stopAnimating()
            }
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    func MakePostAPIWithoutVC(name:String, params:[String:Any], completionHandler: @escaping (NSDictionary?, String?)-> Void) {

        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available")
            return
        }
        
        let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-Type": "application/json"]
        
        let url = BASE_URL + name
        print(url)
        print(params)
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            print(response)
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    //MARK: - Get
    func MakeGetAPI(name:String, params:[String:Any], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-Type": "application/json"]
        print(headers)
        let url = BASE_URL + name
        print(url)
        print(params)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            
            if progress {
                vc.stopAnimating()
            }
//            print(response)

            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    
    //MARK: - Put
    func MakePutAPI(name:String, params:[String:Any], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void) {

        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        let url = BASE_URL + name
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
//            print(response)
            
            if progress {
                vc.stopAnimating()
            }
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    //MARK: - Post with Image upload
    func MakePostWithImageAPI(name:String, params:[String:Any], images:[UIImage], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void)
    {
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        var headers:[String : String] = [:]
       /* if getUserDetail().success != nil {
            let base64Credentials = (getUserDetail().data?.tokenType)! + " " + (getUserDetail().data?.accessToken)!
            headers = ["Authorization": base64Credentials, "Content-Type": "application/json"]
        }
        else {
            headers = ["Content-Type": "application/json"]
        }*/
        print(headers)
        
        let url = BASE_URL + name
        print(url)
        print(params)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for image in images {
                multipartFormData.append(image.jpeg(.medium)!, withName: "photo",fileName: "\(randomString(length: 5)).jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            
            print(multipartFormData)
        }, to: url, method:.post,
           headers:headers, encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    if progress {
                        vc.stopAnimating()
                    }
//                    print(response)
                    
                    if(response.error == nil) {                       
                        completionHandler(response.result.value as? NSDictionary, nil)
                    }
                    else {
                        completionHandler(nil, SERVER_VALIDATION)
                    }
                })
            case .failure( _):
                completionHandler(nil, SERVER_VALIDATION)
            }
        })
    }
}
