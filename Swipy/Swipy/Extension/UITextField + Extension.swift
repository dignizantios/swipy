//
//  UITextField + Extension.swift
//  RentASki
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

extension CustomTextField
{
    func setThemeTextFieldUI()
    {
        self.clipsToBounds = true
        self.cornerRadius = self.bounds.height/2
        self.font = themeFont(size: 16, fontname: .regular)
        self.textColor = UIColor.black
        self.tintColor = UIColor.appBlackColor
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth  = 1.0
    }
    
}
