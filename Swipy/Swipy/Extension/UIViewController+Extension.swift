//
//  UIViewController+Extension.swift
//  RentASki
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
//import SideMenu

extension UIViewController : NVActivityIndicatorViewable
{
    
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appBlueColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: getCommonString(key: "Btn_Done_key"), style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    func addInputAccessoryForTextFields(textFields: [UITextField], dismissable: Bool = true, previousNextable: Bool = false) {
        for (index, textField) in textFields.enumerated()
        {
            let toolbar: UIToolbar = UIToolbar()
            toolbar.barStyle = .default
            toolbar.tintColor = UIColor.appBlueColor
            toolbar.isTranslucent = true
            toolbar.sizeToFit()
            
            var items = [UIBarButtonItem]()
            if previousNextable
            {
                let previousButton = UIBarButtonItem(image: UIImage(named: "ic_top_arrow"), style: .plain, target: nil, action: nil)
                previousButton.width = 30
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                let nextButton = UIBarButtonItem(image: UIImage(named: "ic_down_arrow"), style: .plain, target: nil, action: nil)
                nextButton.width = 30
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextField.becomeFirstResponder)
                }
                items.append(contentsOf: [previousButton, nextButton])
            }
            
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing))
            items.append(contentsOf: [spacer, doneButton])
            
            
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
    
    
    //MARK:- Navigation Bar Setup
    
    func setUpNavigationBarWithTitle(strTitle : String,isTitleInCenter:Bool = true,isLeftButtonShow:Bool = true,backgroundColor:UIColor = UIColor.white,isForInvoices:Bool = false)
    {
      
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
      // setNavigationShadow()
        
        var HeaderLabel = UILabel()
        
        if isTitleInCenter{
            HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
            HeaderLabel.textAlignment = .center
        }else{
            HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 290, height: 35))
        }
        
        if backgroundColor == UIColor.white{
            HeaderLabel.textColor = UIColor.appBlueColor
            HeaderLabel.font = themeFont(size: 16, fontname: .regular)
        }else{
            HeaderLabel.textColor = UIColor.white
            HeaderLabel.font = themeFont(size: 16, fontname: .bold)
        }
        
        if isForInvoices{
            HeaderLabel.textColor = UIColor.appBlueColor
            HeaderLabel.font = themeFont(size: 16, fontname: .regular)
        }
        
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.numberOfLines = 2
        
        self.navigationItem.titleView = HeaderLabel
        self.navigationItem.hidesBackButton = true
        
        if isLeftButtonShow
        {
            
            var imgBack = UIImage()
            
            if GlobalVariables.isEnglish{
                imgBack = UIImage(named: "ic_back") ?? imgBack
            }else{
                imgBack = UIImage(named: "ic_back_roatate") ?? imgBack
            }
            
            let leftButton = UIBarButtonItem(image: imgBack, style: .plain, target: self, action: #selector(backButtonAction))
            if backgroundColor == UIColor.white{
                leftButton.tintColor = UIColor.appBlueColor
            }else{
                leftButton.tintColor = UIColor.white
            }
            self.navigationItem.leftBarButtonItem = leftButton
        }
        
    }
    func setUpNavigationBarBGTitleAndBack(strTitle : String) {
        self.navigationController?.navigationBar.setBackgroundImage(navImage, for: .default)
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appBlueColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        //setNavigationShadow()
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_forgot_password_back_arrow_white"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 18, fontname: .regular)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    
    func setNavigationBarTransparent()
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
    }
    
    func setNavigationShadow()
    {
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    @objc func MenuButtonAction()
    {
        /*let sideMenuNavigationController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        //YourViewController(nibName: "YourViewController", bundle: nil)
       sideMenuNavigationController.settings = setupSideMenu()
        //self.navigationController?.pushViewController(sideMenuNavigationController, animated: true)
        present(sideMenuNavigationController, animated: true, completion: nil)*/
    }

    @objc func backButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func LogoutButtonAction()
    {
        let alertController = UIAlertController(title: getCommonString(key: "Music_Teachers_Helper_key"), message: getCommonString(key: "Are_you_sure_want_to_logout_?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.logoutApi()

        }
        
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }    
    func removeDefaultValue(){
        Defaults.removeObject(forKey: is_firsttime_open_key)
        Defaults.synchronize()
        
        removeUserData()
        SideMenuSelectedIndex = 0
        /*let splashVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigation = UINavigationController(rootViewController: splashVC)
        appDelegate.window?.rootViewController = navigation*/
    }
   
    //MARK: - Loader
    
    
    func showLoader()
    {
        //let LoaderString:String = "Loading..."
        let LoaderSize = CGSize(width: 50, height: 50)
        
        startAnimating(LoaderSize, message: nil, type: NVActivityIndicatorType.ballPulseSync)
        
    }
    
    func hideLoader()
    {
        stopAnimating()
    }
    //MARK:- key board hide
    
    func hidekeyBoardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    //MARK: - Validation email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
 
    //MARK: - Date and Time Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = OrignalFormatter
        guard let convertedDate = dateformatter.date(from: strDate) else {
            return ""
        }
        dateformatter.dateFormat = YouWantFormatter
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
        
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
    //MARK:- Logout API Call
    func logoutApi() {
        /*let parameters : [String : Any] =   [ APIKey.key_token:getUserData()?.message?.token ?? ""
        ]
        print("parameters:", parameters)
        Utility.shared.showActivityIndicator()
        
        self.logoutData(parameter: parameters, success: { (loginData) in
            
            Utility.shared.stopActivityIndicator()
            makeToast(strMessage: getCommonString(key: "Logout_sucessfully_msg_key"))
            self.removeDefaultValue()
            
        }, failed: { (error) in
            Utility.shared.stopActivityIndicator()
            if error == getCommonString(key: "token_key"){
                makeToast(strMessage: getCommonString(key: "Session_expired_msg_key"))
                self.removeDefaultValue()
            }else{
                makeToast(strMessage: error)
            }
        })*/
    }
    /*
    func logoutData(parameter:[String:Any], success:@escaping(_ token : SignupModelClass) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(getCommonString(key: "No_internet_connection_key"))
            return
        }
        WebServices().post(endpoint: LOGOUT, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[getCommonString(key: "key_success")].intValue == 1 {
                    if let data = value.dictionaryObject {
                        
                        if let categoryList = SignupModelClass(JSON: data) {
                            success(categoryList)
                            return
                        }
                    }
                    return
                    
                }
                let msgArray = value[getCommonString(key: "key_msg")].arrayValue
                var msg = String()
                msgArray.forEach { (msgs) in
                    msg += msgs.stringValue
                }
                let fieldsArray = value[getCommonString(key: "key_fields")].arrayValue
                var fields = String()
                fieldsArray.forEach { (field) in
                    if field.stringValue == getCommonString(key: "token_key"){
                        fields = getCommonString(key: "token_key")
                    }
                }
                if fields == getCommonString(key: "token_key"){
                    failed(fields)
                }else{
                    failed(msg)
                }
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(getCommonString(key: "No_internet_connection_key"))
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(getCommonString(key: "Request_time_out_Please_try_again"))
                } else {
                    // other failures
                    failed(getCommonString(key: "Something_went_wrong_key"))
                }
                Utility.shared.stopActivityIndicator()
                break
            }
        }
    }
    */
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
