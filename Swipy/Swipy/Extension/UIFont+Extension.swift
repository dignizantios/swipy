//
//  UIFont+Extension.swift
//  RentASki
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case regular = "Poppins-Regular"
    case bold = "Roboto-Bold"
    case light = "Poppins-Light"
    
}


func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
