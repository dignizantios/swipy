//
//  Date + Extension.swift
//  RentASki
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        print("start - ",start)
        print("end - ",end)
        return end - start
    }
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    func dateByAddingDays(inDays:NSInteger)->Date{
        let date = self
        return Calendar.current.date(byAdding: .day, value: inDays, to: date)!
    }
    
    //minutes difference
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    func dayOfTheWeek() -> String? {
        let weekdays = [
            "\(getCommonString(key: "Sun_key"))",
            "\(getCommonString(key: "Mon_key"))",
            "\(getCommonString(key: "Tue_key"))",
            "\(getCommonString(key: "Wed_key"))",
            "\(getCommonString(key: "Thurs_key"))",
            "\(getCommonString(key: "Fri_key"))",
            "\(getCommonString(key: "Sat_key"))"
        ]
        
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        let components: NSDateComponents = calendar.components(.weekday, from: self) as NSDateComponents
        return weekdays[components.weekday - 1]
    }
    func DateToString(Formatter : String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedString = dateformatter.string(from: self)
        return convertedString
    }
    
    
}
func StringToDate(Formatter : String,strDate : String) -> Date
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    
    guard let convertedDate = dateformatter.date(from: strDate) else {
        let str = dateformatter.string(from: Date())
        return dateformatter.date(from: str)!
    }
    //print("convertedDate - ",convertedDate)
    return convertedDate
}
func StringToDate(strDt:String) -> Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let date:Date = dateFormatter.date(from: strDt)!
    return date
}
func StringToTime(strDt:String) -> Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    let date:Date = dateFormatter.date(from: strDt)!
    return date
}

func generateDatesArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[Date]
{
    var datesArray: [Date] =  [Date]()
    var startDate = startDate
    let calendar = Calendar.current
    
    while startDate <= endDate {
        datesArray.append(startDate)
        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
    }
    return datesArray
}

