//
//  UIButton+Extension.swift
//  RentASki
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit

extension UIButton
{
    func setThemeButtonUI()
    {
        self.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        self.setTitleColor(UIColor.white, for: .normal)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.appBlueColor
        
    }
    
}
