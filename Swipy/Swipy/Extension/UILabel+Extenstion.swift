//
//  UILabel+Extenstion.swift
//  Swipy
//
//  Created by Abhay on 27/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    
    func setSubTextColor(pSubString : String, pColor : UIColor){
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!);
        let range = attributedString.mutableString.range(of: pSubString, options:NSString.CompareOptions.caseInsensitive)
        if range.location != NSNotFound {
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: pColor, range: range);
        }
        self.attributedText = attributedString
        
    }
}
