//
//  UIColor+Extension.swift
//  RentASki
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
   // static var appThemeDarkGrayColor: UIColor { return UIColor.init(red: 224/255, green: 224/255, blue: 225/255, alpha: 1.0) }
    static var appBackgroundColor: UIColor { return UIColor.init(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0) }

    static var appBlueColor: UIColor { return UIColor.init(red: 30/255, green: 52/255, blue: 91/255, alpha: 1.0) }
    
    static var appGreenColor: UIColor { return UIColor.init(red: 0/255, green: 168/255, blue: 91/255, alpha: 1.0) }
    
    static var appBlackColor: UIColor { return UIColor.init(red: 21/255, green: 28/255, blue: 42/255, alpha: 1.0) }//1e345b
    
    static var appDarkGrayColor: UIColor { return UIColor.init(red: 127/255, green: 127/255, blue: 127/255, alpha: 1.0) }
    
    static var appGrayColor: UIColor { return UIColor.init(red: 131/255, green: 144/255, blue: 165/255, alpha: 1.0) }
    
    static var appCyanBatliColor: UIColor { return UIColor.init(red: 19/255, green: 196/255, blue: 187/255, alpha: 1.0) }
    
    static var appRedColor: UIColor { return UIColor.init(red: 215/255, green: 24/255, blue: 45/255, alpha: 1.0) }
}
