//
//  DatePickerVC.swift
//  Swipy
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit

protocol datePickerDelegate {
    func setDateValue(dateValue : Date)
}

struct datepickerMode
{
    var pickerMode : UIDatePicker.Mode
    
    init(customPickerMode:UIDatePicker.Mode)
    {
        self.pickerMode = customPickerMode
    }
}

class DatePickerVC: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet var vwButtonHeader : UIView!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var datePicker : UIDatePicker!
    @IBOutlet weak var vwTappedGesture: UIView!
    
    //MARK: - Variables
    var datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
    var pickerDelegate : datePickerDelegate?
    var isSetMaximumDate = false
    var isSetMinimumDate = false

    var maximumDate = Date()
    var minimumDate = Date()
//    var isSetDate = false
//    var setDateValue : Date?
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SetupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - Setup UI
extension DatePickerVC
{
    func SetupUI()
    {
        [btnDone,btnCancel].forEach({
            $0?.setTitleColor(UIColor.white, for: .normal)
            $0?.titleLabel?.font = themeFont(size: 16, fontname:.regular)
        })
        
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        
        vwButtonHeader.backgroundColor = UIColor.appBlueColor
        
        datePicker.datePickerMode = datePickerCustomMode.pickerMode
        datePicker.date = Date()
        
        if(isSetMaximumDate)
        {
            datePicker.maximumDate = maximumDate
        }
        if(isSetMinimumDate)
        {
            datePicker.minimumDate = minimumDate
        }
        
//        if(isSetDate)
//        {0
//            datePicker.setDate(setDateValue ?? Date(), animated: false)
//        }
//
//        if datePickerCustomMode.pickerMode == .time
//        {
//            datePicker.minuteInterval = 30
//        }
        
        let tappedGesture = UITapGestureRecognizer(target: self, action: #selector(btnCancelAction(_:)))
        self.vwTappedGesture.addGestureRecognizer(tappedGesture)
        
    }

}
//MARK: - Button Action
extension DatePickerVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        pickerDelegate?.setDateValue(dateValue: datePicker.date)
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
