//
//  CustomCommonPicker.swift
//  Swipy
//
//  Created by Haresh on 25/06/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

class CustomCommonPicker: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var vwColor: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var stringPicker: UIPickerView!
    
    
    //MARK: Variables
    var allVehicleArray = NSArray()
    var selectYearArray = NSArray()
    
    
    var customHandlor : (String) -> Void = {weak in}
    
    var titleString = String()
    
   // var selectedEaring = selectEarning.vehicle
    
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        [btnDone, btnCancel].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 17.0, fontname: .regular)
            btn?.setTitleColor(UIColor.white, for: .normal)
        }
        
        btnDone.setTitle(getCommonString(key: "Btn_Done_key"), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Btn_Cancel_key"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) { // As soon as vc appears
        super.viewWillAppear(true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) { // As soon as vc disappears
        super.viewWillDisappear(true)
    }
    
}


//MARK: Button Action
extension CustomCommonPicker {
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        
        let index = self.stringPicker.selectedRow(inComponent: 0)
       /* if selectedEaring == .vehicle {
            self.customHandlor(allVehicleArray.object(at: index) as! String)
        } else {
            self.customHandlor(selectYearArray.object(at: index) as! String)
        }*/
        self.dismiss(animated: true, completion: nil)
    }

}


//MARK: PIckerDelegate
extension CustomCommonPicker : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        /*if selectedEaring == .vehicle{
            return allVehicleArray.count
        }*/
        return selectYearArray.count

        /*if self.allVehicleArray.count != 0 {
            return allVehicleArray.count
        }
        else if self.selectYearArray.count != 0 {
            return selectYearArray.count
        }
        return 0*/
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        /*if selectedEaring == .vehicle {
            return allVehicleArray[row] as? String
        }*/
        return selectYearArray[row] as? String
        
        /*
        if self.allVehicleArray.count != 0 {
            
            return allVehicleArray[row] as? String
        }
        else if self.selectYearArray.count != 0 {
            
            return selectYearArray[row] as? String
        }
        */
    }
    
    /* func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if self.allVehicleArray.count != 0 {
            
            titleString.removeAll()
            titleString = allVehicleArray[row] as! String
            print("\(allVehicleArray[row])")
        }
        else if self.selectYearArray.count != 0 {
            
            titleString.removeAll()
            titleString = selectYearArray[row] as! String
            print("\(selectYearArray[row])")
        }
    } */
    
}
